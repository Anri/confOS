#!/usr/bin/env bash

set -o errexit  # crash the script when a command crash
set -o pipefail # same as above for piped command
set -o nounset  # crash when a variable doesnt exist

# TRACE=1 for debug
if [[ "${TRACE-0}" == "1" ]]; then
  set -o xtrace
fi

cd "$(dirname "$0")" # change script directory

function main {
  # === Variables ==
  local repo gs_mediakey shortcut
  repo=https://git.mylloon.fr/Anri/confOS/raw/branch/main
  gs_mediakey=org.gnome.settings-daemon.plugins.media-keys
  shortcut=/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings


  # === Assure that keyring is installed ==
  pacman -D --asexplicit archlinux-keyring # keep this package


  # === Remove extra packages from Gnome default + useless dependencies ==
  sudo pacman -Rsn --noconfirm \
    epiphany gnome-maps cheese gnome-weather gnome-music gnome-books vim \
    gnome-boxes gnome-photos malcontent gnome-contacts totem gedit
    "$(pacman -Qqtd)"


  # === Global OS configuration set-up ==
  curl ${repo}/meta.sh -s | bash -s -- ${repo}


  # === Add pacman hooks ==
  sudo mkdir -p /etc/pacman.d/hooks
  local hooks
  hooks=("stop-wine-associations" "gnome-terminal-shortcut" "blackbox-shortcut"
         "honkai-shortcut" "vesktop")
  for hook in "${hooks[@]}"
  do
    sudo wget -q --show-progress ${repo}/pacman.d/hooks/"$hook".hook -O /etc/pacman.d/hooks/"$hook".hook
  done


  # === AUR packages ==
  paru -S --noconfirm \
    obsidian parsec-bin evolution-etesync-git x11-emoji-picker ufw-docker \
    gnome-shell-extension-dash-to-dock gnome-shell-extension-tiling-assistant \
    dracula-icons-git vscodium-bin otf-symbola touchegg evdi displaylink \
    gnome-shell-extension-nightthemeswitcher nerd-fonts-meslo tex-match \
    gnome-shell-extension-x11gestures gnome-shell-extension-no-overview \
    gnome-shell-extension-gsconnect org.gnome.shell.extensions.color-picker \
    gnome-shell-extension-rounded-window-corners-reborn blackbox-terminal \
    gnome-shell-extension-alphabetical-grid-extension texlive-latexindent-meta \
    gnome-shell-extension-quick-settings-tweaks-git gpu-screen-recorder \
    epson-inkjet-printer-stylus-photo-px810fw-series heroic-games-launcher-bin \
    gnome-shell-extension-blur-my-shell clamav-unofficial-sigs vesktop \
    nautilus-open-any-terminal tuned-ppd eloquent youtube-music-bin


  # === Disable wayland ==
  sed -i "s/#Wayland/Wayland/g" /etc/gdm/custom.conf


  # === Locales ==
  sudo sed -i "s/fr_FR.UTF-8.UTF-8 UTF-8/fr_FR.UTF8 UTF-8/g" /etc/locale.gen
  sudo locale-gen
  localectl set-locale fr_FR.UTF-8


  # === Fish ==
  # Custom Arch configuration
  curl -s ${repo}/.config/fish/conf.d/abbr_arch.fish >> "$HOME"/.config/fish/conf.d/abbr.fish
  curl -s ${repo}/.config/fish/conf.d/alias_arch.fish >> "$HOME"/.config/fish/conf.d/alias.fish

  # Themes in Gnome Terminal, the default one will be the dark one
  local GT_default
  GT_default=$(gsettings get org.gnome.Terminal.ProfilesList default | tr -d \')
  wget -q ${repo}/.config/update_theme.sh -O "$HOME"/.config/update_theme.sh
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ use-custom-command true
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ custom-command "fish"
  # Default colors
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ use-theme-colors "false"
  # Size and font
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ use-system-font false
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ default-size-columns 105
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ default-size-rows 25
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ font "MesloLGM Nerd Font 12"
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ use-system-font false
  # Theme (use dark for the purpose of the installation since we need to choose one, doesn't really matter)
  bash "$HOME"/.config/update_theme.sh sunset

  # Keybinds
  gsettings set org.gnome.Terminal.Legacy.Keybindings:/org/gnome/terminal/legacy/keybindings/ close-tab "<Control>w"
  gsettings set org.gnome.Terminal.Legacy.Keybindings:/org/gnome/terminal/legacy/keybindings/ new-tab "<Control>t"
  gsettings set org.gnome.Terminal.Legacy.Keybindings:/org/gnome/terminal/legacy/keybindings/ zoom-in "<Control>equal" # ctrl++
  gsettings set org.gnome.Terminal.Legacy.Keybindings:/org/gnome/terminal/legacy/keybindings/ zoom-normal "<Control>agrave" # ctrl+0
  gsettings set org.gnome.Terminal.Legacy.Keybindings:/org/gnome/terminal/legacy/keybindings/ next-tab "<Control>tab" # ctrl+tab


  # === Syncthing ==
  systemctl enable syncthing@"$USER".service
  systemctl start syncthing@"$USER".service


  # === Flameshot ==
  # Custom configuration
  wget -q --show-progress ${repo}/.config/flameshot/flameshot.ini -O "$HOME"/.config/flameshot/flameshot.ini
  # Change keybindings
  gsettings set org.gnome.shell.keybindings show-screenshot-ui "[]"
  local custom_keybindings_list flameshot_shortcuts
  custom_keybindings_list=$(gsettings get ${gs_mediakey} custom-keybindings)
  flameshot_shortcuts="'${shortcut}/flameshot-shortcut/', '${shortcut}/flameshot-shortcut-2/'"
  if [[ $custom_keybindings_list == "@as []" ]] # creating new list if not exists
  then
    custom_keybindings_list="[${flameshot_shortcuts}]"
  else # if already existing, adding our new element
    custom_keybindings_list="${custom_keybindings_list::-1}, ${flameshot_shortcuts}]"
  fi
  gsettings set ${gs_mediakey} custom-keybindings "$custom_keybindings_list"
  # Shortcut 1
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/flameshot-shortcut/ name "Flameshot" # set name
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/flameshot-shortcut/ command "flameshot gui" # set command
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/flameshot-shortcut/ binding "Print" # set key
  # Shortcut 2
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/flameshot-shortcut-2/ name "Flameshot Windows style" # set name
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/flameshot-shortcut-2/ command "flameshot gui" # set command
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/flameshot-shortcut-2/ binding "<Shift><Super>s" # set key


  # === Emoji picker ==
  # Custom configuration
  wget -q --show-progress ${repo}/.config/gazatu.xyz/emoji-picker.ini -O "$HOME"/.config/gazatu.xyz/emoji-picker.ini
  # Add keybinding
  custom_keybindings_list=$(gsettings get ${gs_mediakey} custom-keybindings)
  if ! echo "$custom_keybindings_list" | grep -q "x11-emoji-picker"
  then
    if [ "$custom_keybindings_list" = "@as []" ] # creating new list if not exists
      then
        custom_keybindings_list="['${shortcut}/x11-emoji-picker/']"
      else # if already existing, adding our new element
        custom_keybindings_list=$(echo "$custom_keybindings_list" | sed "s/\[//" | sed "s/\]//")
        custom_keybindings_list="[$custom_keybindings_list, '${shortcut}/x11-emoji-picker/']"
    fi
    gsettings set ${gs_mediakey} custom-keybindings "$custom_keybindings_list" # update the list
    gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/x11-emoji-picker/ name "Emoji-Picker" # set name
    gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/x11-emoji-picker/ command "emoji-picker" # set command
    gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/x11-emoji-picker/ binding "<Super>period" # set shortcut
  fi


  # === Gnome configuration ==
  # Enable location
  gsettings set org.gnome.system.location enabled true

  # Theme (dark by default)
  gsettings set org.gnome.desktop.interface color-scheme prefer-dark
  gsettings set org.gnome.desktop.interface icon-theme "Dracula"

  # Extensions
  killall -HUP gnome-shell # restart gnome
  gsettings set org.gnome.shell disable-user-extensions false # enable extensions globally
  gsettings set org.gnome.shell disable-extension-version-validation true # legacy extension support
  gnome-extensions enable dash-to-dock@micxgx.gmail.com
  gnome-extensions enable tiling-assistant@leleat-on-github
  gnome-extensions enable x11gestures@joseexposito.github.io
  gnome-extensions enable nightthemeswitcher@romainvigier.fr
  gnome-extensions enable quick-settings-tweaks@qwreey
  gnome-extensions enable appindicatorsupport@rgcjonas.gmail.com
  gnome-extensions enable AlphabeticalAppGrid@stuarthayhurst
  gnome-extensions enable ding@rastersoft.com
  gnome-extensions enable color-picker@tuberry
  gnome-extensions enable gsconnect@andyholmes.github.io
  gnome-extensions enable blur-my-shell@aunetx
  gnome-extensions enable rounded-window-corners@fxgn
  gnome-extensions enable caffeine@patapon.info
  gnome-extensions enable no-overview@fthx

  # Nautilus keybinding
  custom_keybindings_list=$(gsettings get ${gs_mediakey} custom-keybindings)
  if [[ $custom_keybindings_list == "@as []" ]] # creating new list if not exists
  then
    custom_keybindings_list="['${shortcut}/nautilus-shortcut/']"
  else # if already existing, adding our new element
    custom_keybindings_list="${custom_keybindings_list::-1}, '${shortcut}/nautilus-shortcut/']"
  fi
  gsettings set ${gs_mediakey} custom-keybindings "$custom_keybindings_list" # updating the list
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/nautilus-shortcut/ name "Nautilus" # set name
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/nautilus-shortcut/ command "nautilus -w" # set command
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/nautilus-shortcut/ binding "<Super>E" # set shortcut

  # Some Gnome configuration
  gsettings set org.gnome.shell.extensions.dash-to-dock click-action minimize # add minimized window on dock click
  gsettings set org.gnome.shell.extensions.dash-to-dock show-trash false # add Dash to Dock
  gsettings set org.gnome.shell.extensions.dash-to-dock show-apps-at-top true # apps button at left
  gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true # show on all screens
  gsettings get org.gnome.shell.extensions.blur-my-shell.panel blur false # don't blur the topbar
  gsettings set org.gnome.desktop.wm.preferences button-layout ":minimize,maximize,close" # affiche les boutons maximisé et minimisé
  gsettings set org.gnome.desktop.wm.preferences audible-bell false # remove sound of notification when going too far away in a window
  gsettings set org.gnome.desktop.wm.keybindings close "['<Super>Q', '<Alt>F4']" # Alt-F4 keybinding
  gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Alt>Tab']" # alt-tab for windows
  gsettings set org.gnome.desktop.wm.keybindings switch-windows-backward "['<Shift><Alt>Tab']" # same as above but backword
  gsettings set org.gnome.desktop.wm.keybindings switch-applications "['<Super>Tab']" # windows-tab for applications
  gsettings set org.gnome.desktop.wm.keybindings switch-applications-backward "['<Shift><Super>Tab']" # same as above but backword
  gsettings set org.gnome.desktop.interface clock-show-seconds true # affiche les secondes dans l'horloge
  gsettings set org.gnome.desktop.interface show-battery-percentage true # show battery poucentage
  gsettings set org.gnome.desktop.interface enable-hot-corners false # disable top-right corner "activities"
  gsettings set org.gnome.desktop.background show-desktop-icons true # show icons on desktop
  gsettings set org.gnome.nautilus.preferences show-image-thumbnails "always" # thumbnails nautilus
  gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type "nothing" # no sleep on AC
  gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true # mode "nuit" (lumière bleue)
  gsettings set org.gnome.desktop.peripherals.touchpad click-method areas # right click on laptop
  gsettings set org.gnome.desktop.peripherals.mouse speed -0.6 # mouse speed
  gsettings set org.gnome.desktop.peripherals.touchpad send-events disabled-on-external-mouse # disable touchpad when mouse connected
  gsettings --schemadir /usr/share/gnome-shell/extensions/nightthemeswitcher@romainvigier.fr/schemas/ set org.gnome.shell.extensions.nightthemeswitcher.gtk-variants enabled true # enable GTK theme to change
  gsettings --schemadir /usr/share/gnome-shell/extensions/nightthemeswitcher@romainvigier.fr/schemas/ set org.gnome.shell.extensions.nightthemeswitcher.commands sunset "bash $HOME/.config/update_theme.sh sunset" # run script for the sunset
  gsettings --schemadir /usr/share/gnome-shell/extensions/nightthemeswitcher@romainvigier.fr/schemas/ set org.gnome.shell.extensions.nightthemeswitcher.commands sunrise "bash $HOME/.config/update_theme.sh sunrise" # run script for the sunrise
  gsettings --schemadir /usr/share/gnome-shell/extensions/nightthemeswitcher@romainvigier.fr/schemas/ set org.gnome.shell.extensions.nightthemeswitcher.color-scheme day "prefer-light" # use light theme for the day, instead of "default"
  gsettings set org.gnome.SessionManager logout-prompt false # remove pop up when logging out


  # Default app Nautilus
  sed -i "s/inode\/directory=nautilus.desktop/inode\/directory=org.gnome.Nautilus.desktop/g" "$HOME"/.config/mimeapps.list

  # Configuration TextEditor
  gsettings set org.gnome.TextEditor style-scheme "kate-dark"
  gsettings set org.gnome.TextEditor indent-style "space"
  gsettings set org.gnome.TextEditor tab-width "uint32 4"
  gsettings set org.gnome.TextEditor show-map true
  gsettings set org.gnome.TextEditor show-line-numbers true

  # Configuration Blackbox
  wget -q --show-progress ${repo}/.local/share/blackbox/user-keymap.json -O "$HOME"/.local/share/blackbox/user-keymap.json # keymap # TODO: Quid de mkdir avant?
  gsettings set com.raggesilver.BlackBox use-custom-command true # custom shell
  gsettings set com.raggesilver.BlackBox custom-shell-command "fish" # use fish as default
  gsettings set com.raggesilver.BlackBox easy-copy-paste true # allow ^C and ^V
  gsettings set com.raggesilver.BlackBox remember-window-size true
  gsettings set com.raggesilver.BlackBox window-height 600 # initial height
  gsettings set com.raggesilver.BlackBox window-width 1000 # initial width
  gsettings set com.raggesilver.BlackBox terminal-bell false # disable bell
  gsettings set com.raggesilver.BlackBox theme-dark "Dracula" # dark theme
  gsettings set com.raggesilver.BlackBox theme-light "Solarized Light" # light theme
  gsettings set com.raggesilver.BlackBox working-directory-mode 1 # working dir is home dir
  gsettings set com.raggesilver.BlackBox notify-process-completion false # disable notifiction on each command
  gsettings set com.github.stunkymonkey.nautilus-open-any-terminal terminal "blackbox" # blackbox context menu in nautilus

  # Configuration Color Picker
  gsettings set org.gnome.shell.extensions.color-picker enable-systray false # disable icon in systray
  gsettings set org.gnome.shell.extensions.color-picker enable-shortcut true # enable shortcut
  gsettings set org.gnome.shell.extensions.color-picker color-picker-shortcut "['<Super>C']" # define shortcut

  # Add gesture and tap on touchpad
  gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
  systemctl enable touchegg.service
  systemctl start touchegg


  # === Clipboard manager ==
  gsettings set org.gnome.shell.keybindings toggle-message-tray "[]"
  custom_keybindings_list=$(gsettings get ${gs_mediakey} custom-keybindings)
  if [[ $custom_keybindings_list == "@as []" ]] # creating new list if not exists
  then
    custom_keybindings_list="['${shortcut}/clipboard-shortcut/']"
  else # if already existing, adding our new element
    custom_keybindings_list="${custom_keybindings_list::-1}, '${shortcut}/clipboard-shortcut/']"
  fi
  gsettings set ${gs_mediakey} custom-keybindings "$custom_keybindings_list" # updating the list
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/clipboard-shortcut/ name "Copyq" # set name
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/clipboard-shortcut/ command "copyq show" # set command
  gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/clipboard-shortcut/ binding "<Super>V" # set shortcut
  copyq > /dev/null & disown
  copyq config autostart true
  echo "style_main_window=true" >> /usr/share/copyq/themes/dark.ini
  copyq loadTheme /usr/share/copyq/themes/dark.ini


  # === UBW (pare-feu) ==
  ufw enable


  # === Firefox ==
  firefox &
  sleep 2
  pkill firefox
  wget -q --show-progress ${repo}/.mozilla/firefox/user.js -P "$HOME"/.mozilla/firefox/*.default-release
  curl -s ${repo}/.mozilla/firefox/user-arch.js >> "$HOME"/.mozilla/firefox/*.default-release/user.js
  # 2 fingers history control
  echo "export MOZ_USE_XINPUT2=1" >> "$HOME"/.profile
  # Hardware acceleration
  echo "export MOZ_DRM_DEVICE=/dev/dri/renderD128" >> "$HOME"/.profile
  # Download extension configuration (TODO: Check Download Folder's Name)
  #wget -q --show-progress ${repo}/.mozilla/firefox/extensions/Dark-Reader-Settings.json -O "$HOME"/Téléchargements/Dark-Reader-Settings.json
  #wget -q --show-progress ${repo}/.mozilla/firefox/extensions/SponsorBlockConfig.json -O "$HOME"/Téléchargements/SponsorBlockConfig.json
  #wget -q --show-progress ${repo}/.mozilla/firefox/extensions/auto-tab-discard-preferences.json -O "$HOME"/Téléchargements/auto-tab-discard-preferences.json


  # === Docker ==
  systemctl enable docker.service
  systemctl start docker.service
  usermod -aG docker "$USER"


  # === Unused icons ==
  apps=("fish" "cmake-gui" "com.github.hluk.copyq" "electron17" "htop" "yelp"
        "avahi-discover" "bssh" "bvnc" "org.flameshot.Flameshot" "lstopo"
        "org.fontforge.FontForge" "micro" "qv4l2" "qvidcap" "org.gnome.Evince"
        "x11-emoji-picker" "xdvi")
  for app in "${apps[@]}"
  do
    if [ -f /usr/share/applications/"$app".desktop ]
    then
      mv /usr/share/applications/"$app".desktop /usr/share/applications/"$app".desktop.bak
    fi
  done


  # === Enable Bluetooth ==
  systemctl enable bluetooth.service
  systemctl start bluetooth.service
  sed -i "s/#AutoEnable=true/AutoEnable=false/g" /etc/bluetooth/main.conf


  # === VSCodium ==
  wget -q ${repo}/.config/VSCodium/product.json -O "$HOME"/.config/VSCodium/product.json


  # === Discord ==
  # Skip update
  path_config_discord="$HOME"/.config/discord/settings.json
  updated_config_discord="$(< "$path_config_discord" jq '. + { "SKIP_HOST_UPDATE": true }')"
  echo "$updated_config_discord" > "$path_config_discord"


  # === Printers ==
  sudo systemctl enable cups.socket
  sudo systemctl start cups.socket
  sudo systemctl enable avahi-daemon.service
  sudo systemctl start avahi-daemon.service


  # === UFW ==
  sudo systemctl enable ufw.service
  sudo systemctl start ufw.service
  sudo ufw default deny
  sudo ufw allow syncthing
  sudo ufw allow 1714:1764/udp # KDEConnect
  sudo ufw allow 1714:1764/tcp # KDEConnect
  sudo ufw-docker install
  sudo systemctl restart ufw


  # === Optimizations ==
  # Disable NetworkManager-wait-online.service because it add 8s at boot
  sudo systemctl disable NetworkManager-wait-online.service


  # === s-tui ==
  # Configuration
  wget -q --show-progress ${repo}/.config/s-tui/s-tui.conf -O "$HOME"/.config/s-tui/s-tui.conf


  # === ClamAV ==
  # Virus definitions
  sudo systemctl enable clamav-freshclam.service
  sudo systemctl start clamav-freshclam.service
  # Daemon
  sudo systemctl enable clamav-daemon.service
  sudo systemctl start clamav-daemon.service


  # === GPU Screen recorder ==
  # Configuration
  wget -q --show-progress ${repo}/.config/gpu-screen-recorder.env -O "$HOME"/.config/gpu-screen-recorder.env
  # Add service
  wget -q --show-progress https://git.dec05eba.com/gpu-screen-recorder/plain/extra/gpu-screen-recorder.service -O /etc/systemd/system/gpu-screen-recorder.service
  # Enable
  systemctl enable --now gpu-screen-recorder
  # Keybind
  custom_keybindings_list=$(gsettings get ${gs_mediakey} custom-keybindings)
  if ! echo "$custom_keybindings_list" | grep -q "gpu-screen-recorder"
  then
    if [ "$custom_keybindings_list" = "@as []" ] # creating new list if not exists
      then
        custom_keybindings_list="['${shortcut}/gpu-screen-recorder/']"
      else # if already existing, adding our new element
        custom_keybindings_list=$(echo "$custom_keybindings_list" | sed "s/\[//" | sed "s/\]//")
        custom_keybindings_list="[$custom_keybindings_list, '${shortcut}/gpu-screen-recorder/']"
    fi
    gsettings set ${gs_mediakey} custom-keybindings "$custom_keybindings_list" # update the list
    gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/gpu-screen-recorder/ name "Shadowplay" # set name
    gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/gpu-screen-recorder/ command "bash -c \"killall -SIGUSR1 gpu-screen-recorder && notify-send -et 3000 -- 'Shadowplay' '2 dernières minutes sauvés dans ~/Vidéos'\"" # set command
    gsettings set ${gs_mediakey}.custom-keybinding:${shortcut}/gpu-screen-recorder/ binding "<Super>Print" # set shortcut
  fi


  # === DisplayLink ==
  # For screens trough hub
  sudo systemctl enable displaylink.service
  sudo systemctl start displaylink.service
  sudo sed -i "s/#HandleLidSwitchDocked=ignore/HandleLidSwitchDocked=ignore/g" /etc/systemd/logind.conf


  # === Power management ==
  sudo systemctl enable tuned.service
  sudo systemctl start tuned.service
  tuned-adm auto_profile # change the profile automatically


  printf "\nConcernant le Secure Boot, voici le tuto :"
  echo "https://www.reddit.com/r/archlinux/comments/zo83gb/how_i_setup_secure_boot_for_arch_linux_simple/"
  echo -e "\n\nInstallation terminée.\nIl faut redémarrer l'ordinateur."
}

if grep "archlinux" /proc/version > /dev/null;
then
  main "$@"
else
  echo "Ce script ne fonctionne que sur Arch." >&2
fi
