# Version control
VC = git

# Util
RM    = rm -f
MKDIR = mkdir -p
SED   = sed -i

# Downloaders
REPO_SRC = https://git.mylloon.fr/Anri/confOS/raw/branch/main
WGET     = wget -q --show-progress
CURL     = curl -s

# Hide some verbose messages from Make
MAKEFLAGS += --no-print-directory

# Location
WIN_USER     = anri
PFx86        = /mnt/c/Program\ Files\ \(x86\)
PF           = /mnt/c/Program\ Files
WIN_HOME     = /mnt/c/Users/$(WIN_USER)
APPDATA      = $(WIN_HOME)/AppData/Roaming
LOCALAPPDATA = $(WIN_HOME)/AppData/Local

# Locations aliases
CS2_CFG = $(PFx86)/Steam/steamapps/common/Counter-Strike\ Global\ Offensive/game/csgo/cfg
OBS_CFG = $(PF)/obs-studio/config/obs-studio


# Checkers
ARCH      = $(shell grep -c "archlinux" /proc/version 2> /dev/null)
WSL       = $(shell grep -c "WSL2" /proc/version 2> /dev/null)
TERMUX    = $(firstword $(shell type termux-info &> /dev/null && echo 1))
ADMIN_WIN = $(shell touch $(PF) 2> /dev/null; echo $$?)


all: full-sync


# Pull latest changes
refresh:
	@echo "Pull latest changes from repository..."
	@$(VC) pull
	@echo


# Pull and sync
full-sync:
	@$(MAKE) refresh
	@$(MAKE) sync


# .config
sync-dotconfig:
# Fish
	@$(MKDIR) $(HOME)/.config/fish/conf.d
	@$(MKDIR) $(HOME)/.config/fish/functions

	@$(WGET) $(REPO_SRC)/.config/fish/config.fish -O $(HOME)/.config/fish/config.fish

	@$(WGET) $(REPO_SRC)/.config/fish/functions/__fish_list_current_token.fish -O $(HOME)/.config/fish/functions/__fish_list_current_token.fish

	@$(WGET) $(REPO_SRC)/.config/fish/functions/fish_prompt.fish -O $(HOME)/.config/fish/functions/fish_prompt.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/catall.fish -O $(HOME)/.config/fish/functions/catall.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/latex-color.fish -O $(HOME)/.config/fish/functions/latex-color.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/makeaway.fish -O $(HOME)/.config/fish/functions/makeaway.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/ssh-export.fish -O $(HOME)/.config/fish/functions/ssh-export.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/ssh-import.fish -O $(HOME)/.config/fish/functions/ssh-import.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/ugc.fish -O $(HOME)/.config/fish/functions/ugc.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/update-theme.fish -O $(HOME)/.config/fish/functions/update-theme.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/remove-path.fish -O $(HOME)/.config/fish/functions/remove-path.fish

	@$(WGET) $(REPO_SRC)/.config/fish/conf.d/abbr.fish -O $(HOME)/.config/fish/conf.d/abbr.fish
	@$(WGET) $(REPO_SRC)/.config/fish/conf.d/alias.fish -O $(HOME)/.config/fish/conf.d/alias.fish
	@echo "Fish global settings synced!"
	@echo

# Micro
	@$(MKDIR) $(HOME)/.config/micro
	@$(WGET) $(REPO_SRC)/.config/micro/settings.json -O $(HOME)/.config/micro/settings.json
	@echo "Micro settings synced!"
	@echo

# Bat
	@$(MKDIR) $(HOME)/.config/bat
	@$(WGET) $(REPO_SRC)/.config/bat/config -O $(HOME)/.config/bat/config
	@echo "Bat settings synced!"
	@echo

# Topgrade
	@$(WGET) $(REPO_SRC)/.config/topgrade/topgrade.toml -O $(HOME)/.config/topgrade.toml
	@echo "Topgrade settings synced!"
	@echo


# # .emacs.d
# sync-emacs:
# 	@$(MKDIR) $(HOME)/.emacs.d
#
# 	@$(WGET) $(REPO_SRC)/.emacs.d/init.el -O $(HOME)/.emacs.d/init.el


# .dotfiles
sync-dotfiles:
# .gitconfig
	@$(WGET) $(REPO_SRC)/dotfiles/.gitconfig -O $(HOME)/.gitconfig
	@$(WGET) $(REPO_SRC)/dotfiles/.university.gitconfig -O $(HOME)/.university.gitconfig
	@echo "Git settings synced!"
	@echo

# .ocamlformat
	@$(WGET) $(REPO_SRC)/dotfiles/.ocamlformat -O $(HOME)/.ocamlformat
	@echo "OCaml formatter settings synced!"
	@echo

# .clang-format
	@$(WGET) $(REPO_SRC)/dotfiles/.clang-format -O $(HOME)/.clang-format
	@echo "Clang formatter settings synced!"
	@echo


# Arch distro specific
sync-arch:
# .config
	@$(WGET) $(REPO_SRC)/.config/update_theme.sh -O $(HOME)/.config/update_theme.sh
	@echo "Theme script synced!"
	@echo

# Fish
	@$(CURL) $(REPO_SRC)/.config/fish/conf.d/abbr_arch.fish >> $(HOME)/.config/fish/conf.d/abbr.fish
	@$(CURL) $(REPO_SRC)/.config/fish/conf.d/alias_arch.fish >> $(HOME)/.config/fish/conf.d/alias.fish
	@echo "Fish specific settings synced!"
	@echo

# Emoji Picker
	@$(WGET) $(REPO_SRC)/.config/gazatu.xyz/emoji-picker.ini -O $(HOME)/.config/gazatu.xyz/emoji-picker.ini
	@echo "Emoji picker settings synced!"
	@echo

# Firefox
	-@$(RM) $(HOME)/.mozilla/firefox/*.default-release/user.js
	@$(WGET) $(REPO_SRC)/.mozilla/firefox/user.js -P $(HOME)/.mozilla/firefox/*.default-release
	@$(CURL) $(REPO_SRC)/.mozilla/firefox/user-arch.js >> $(wildcard $(HOME)/.mozilla/firefox/*.default-release)/user.js
	@echo "Firefox settings synced!"
	@echo

# VSCodium
	@$(WGET) $(REPO_SRC)/.config/VSCodium/product.json -O $(HOME)/.config/VSCodium/product.json
	@echo "VSCodium settings synced!"
	@echo

# s-tui
	@$(WGET) $(REPO_SRC)/.config/s-tui/s-tui.conf -O $(HOME)/.config/s-tui/s-tui.conf
	@echo "s-tui settings synced!"
	@echo

# gpu-screen-recorder
	@$(WGET) $(REPO_SRC)/.config/gpu-screen-recorder.env -O $(HOME)/.config/gpu-screen-recorder.env
	@echo "ADDITIONAL_ARGS=-a $$(pactl get-default-source)" >> $(HOME)/.config/gpu-screen-recorder.env
	@echo "gpu-screen-recorder settings synced!"
	@echo

# Blackbox
	@$(WGET) $(REPO_SRC)/.local/share/blackbox/user-keymap.json -O $(HOME)/.local/share/blackbox/user-keymap.json
	@echo "Blackbox keymap synced!"
	@echo

# Pacman hooks
	@sudo $(MKDIR) /etc/pacman.d/hooks
	@sudo $(WGET) $(REPO_SRC)/pacman.d/hooks/stop-wine-associations.hook -O /etc/pacman.d/hooks/stop-wine-associations.hook
	@sudo $(WGET) $(REPO_SRC)/pacman.d/hooks/gnome-terminal-shortcut.hook -O /etc/pacman.d/hooks/gnome-terminal-shortcut.hook
	@sudo $(WGET) $(REPO_SRC)/pacman.d/hooks/blackbox-shortcut.hook -O /etc/pacman.d/hooks/blackbox-shortcut.hook
	@sudo $(WGET) $(REPO_SRC)/pacman.d/hooks/honkai-shortcut.hook -O /etc/pacman.d/hooks/honkai-shortcut.hook
	@sudo $(WGET) $(REPO_SRC)/pacman.d/hooks/vesktop.hook -O /etc/pacman.d/hooks/vesktop.hook
	@echo "Pacman hooks synced!"
	@echo


# Windows Subsystem for Linux specific
sync-wsl:
# Fish
	@$(CURL) $(REPO_SRC)/.config/fish/config_wsl.fish >> $(HOME)/.config/fish/config.fish
	@$(CURL) $(REPO_SRC)/.config/fish/conf.d/abbr_wsl.fish >> $(HOME)/.config/fish/conf.d/abbr.fish
	@$(CURL) $(REPO_SRC)/.config/fish/conf.d/alias_wsl.fish >> $(HOME)/.config/fish/conf.d/alias.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/trash.fish -O $(HOME)/.config/fish/functions/trash.fish
	@$(WGET) $(REPO_SRC)/.config/fish/functions/trash-empty.fish -O $(HOME)/.config/fish/functions/trash-empty.fish
	@echo "Fish specific settings synced!"
	@echo

# Unison
	@$(MKDIR) $(HOME)/.unison
	@$(WGET) $(REPO_SRC)/.unison/default.prf -O $(HOME)/.unison/default.prf
	@echo "Unison settings synced!"
	@echo


# Windows through WSL
WIN = (Windows) # specific prefix since we're running it from WSL for windows
sync-windows:
# Scripts
	@$(WGET) $(REPO_SRC)/windows/update.bat -O $(WIN_HOME)/Documents/Local/update.bat
	@$(WGET) $(REPO_SRC)/windows/clean.bat -O $(WIN_HOME)/Documents/Local/ctmpf.bat
	@$(WGET) $(REPO_SRC)/windows/installers/spotify.bat -O $(WIN_HOME)/Documents/Local/blockthespot.bat
	@$(WGET) $(REPO_SRC)/windows/installers/vencord.bat -O $(WIN_HOME)/Documents/Local/vencord_installer.bat
#	@$(WGET) $(REPO_SRC)/windows/fix_nvidia_wsl.bat -O $(WIN_HOME)/Documents/Local/fix_nvidia_wsl.bat
	@$(WGET) $(REPO_SRC)/windows/start_obs_as_shadowplay.bat -O $(LOCALAPPDATA)/start_obs_as_shadowplay.bat
	@$(WGET) $(REPO_SRC)/windows/reg_fixes.bat -O $(WIN_HOME)/Documents/Local/reg_fixes.bat
	@echo "$(WIN) Scripts synced!"
	@echo

# CS2 (only if installed)
	@if [ -d $(CS2_CFG) ]; then \
		$(WGET) $(REPO_SRC)/games/cs2/autoexec.cfg -O $(CS2_CFG)/autoexec.cfg; \
		$(WGET) $(REPO_SRC)/games/cs2/myconfig.cfg -O $(CS2_CFG)/myconfig.cfg; \
		$(WGET) $(REPO_SRC)/games/cs2/customgame.cfg -O $(CS2_CFG)/customgame.cfg; \
		$(WGET) $(REPO_SRC)/games/cs2/normalgame.cfg -O $(CS2_CFG)/normalgame.cfg; \
		echo "$(WIN) CS2 configs synced!"; \
		echo; \
	fi

# Firefox
	-@$(RM) $(APPDATA)/Mozilla/Firefox/Profiles/*.default-release/user.js
	@$(WGET) $(REPO_SRC)/.mozilla/firefox/user.js -P $(APPDATA)/Mozilla/Firefox/Profiles/*.default-release
	@$(CURL) $(REPO_SRC)/.mozilla/firefox/user-windows.js >> $(wildcard $(APPDATA)/Mozilla/Firefox/Profiles/*.default-release)/user.js
	@echo "$(WIN) Firefox settings synced!"
	@echo

# WindowsAutoNightMode
	@$(WGET) $(REPO_SRC)/.config/WindowsAutoNightMode/config.yaml -O $(APPDATA)/AutoDarkMode/config.yaml
	@$(WGET) $(REPO_SRC)/.config/WindowsAutoNightMode/scripts.yaml -O $(APPDATA)/AutoDarkMode/scripts.yaml
	@echo "$(WIN) WindowsAutoNightMode settings synced!"
	@echo

# Xournal++
	@$(MKDIR) $(LOCALAPPDATA)/xournalpp
	@$(WGET) $(REPO_SRC)/.config/xournalpp/settings.xml -O $(LOCALAPPDATA)/xournalpp/settings.xml
	@echo "$(WIN) Xournal++ settings synced!"
	@echo

# OpenTabletDriver presets
	@$(MKDIR) $(LOCALAPPDATA)/OpenTabletDriver/Presets
	@$(WGET) $(REPO_SRC)/opentabletdriver/vertical-main.json -O $(LOCALAPPDATA)/OpenTabletDriver/Presets/vertical-main.json
	@$(WGET) $(REPO_SRC)/opentabletdriver/vertical-second.json -O $(LOCALAPPDATA)/OpenTabletDriver/Presets/vertical-second.json
	@echo "$(WIN) OpenTabletDriver presets synced!"
	@echo

# # Topgrade
# 	@$(WGET) $(REPO_SRC)/.config/topgrade/topgrade.toml -O $(APPDATA)/topgrade.toml
# 	@echo "$(WIN) Topgrade settings synced!"
# 	@echo

# OBS config
ifeq ($(ADMIN_WIN),1)
	@echo "$(WIN) Can't update OBS configuration, use elevated terminal to do so"
	@echo
else
	@$(MKDIR) $(OBS_CFG)/basic/profiles/ReplayBuffer
	@$(MKDIR) $(OBS_CFG)/basic/scenes
	@$(WGET) $(REPO_SRC)/.config/obs/global.ini -O $(OBS_CFG)/global.ini
	@$(WGET) $(REPO_SRC)/.config/obs/basic/profiles/ReplayBuffer/basic.ini -O $(OBS_CFG)/basic/profiles/ReplayBuffer/basic.ini
	@$(WGET) $(REPO_SRC)/.config/obs/basic/profiles/ReplayBuffer/recordEncoder.json -O $(OBS_CFG)/basic/profiles/ReplayBuffer/recordEncoder.json
	@$(WGET) $(REPO_SRC)/.config/obs/basic/scenes/ReplayBuffer.json -O $(OBS_CFG)/basic/scenes/ReplayBuffer.json
	@echo "$(WIN) OBS configuration synced! Check your OBS to see if the screen is correctly picked up (could have turned black due to the update)"
	@echo
endif


# Termux distro specific
sync-termux:
	@$(SED) "/fish_ssh_agent/d" $(HOME)/.config/fish/config.fish
	@$(SED) "/topgrade/d" $(HOME)/.config/fish/conf.d/abbr.fish
	@echo "Fix-for-Termux applied!"
	@echo


sync:
# Check if at least the platform is supported
ifneq (1, $(filter 1, $(ARCH) $(WSL) $(TERMUX)))
	@echo "❌ Unsupported platform"
else

# Call target of compatible with all supported platforms
	@$(MAKE) sync-dotconfig
	@echo "⌛ .config directory synced!"
	@echo

	@$(MAKE) sync-dotfiles
	@echo "⌛ Dotfiles synced!"
	@echo

# 	@$(MAKE) sync-emacs
# 	@echo "⌛ Emacs settings synced!"
# 	@echo


# Specific to Arch
ifeq ($(ARCH), 1)
	@$(MAKE) sync-arch

	@echo "✅ Arch synced!"
	@echo

# Specific to WSL
else ifeq ($(WSL), 1)
	@$(MAKE) sync-wsl

	@echo "✅ WSL synced!"
	@echo

# Windows
	@$(MAKE) sync-windows

	@echo "✅ Windows synced!"
	@echo

# Specific to Termux
else ifeq ($(TERMUX), 1)
	@$(MAKE) sync-termux

	@echo "✅ Termux synced!"
	@echo
endif

# END
	@echo "🎉 Sync complete!"
endif
