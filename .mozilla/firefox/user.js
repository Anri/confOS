user_pref("browser.contentblocking.category", "strict"); // protection renforcée contre le pistage stricte
user_pref("browser.ctrlTab.sortByRecentlyUsed", true); // navigation entre les onglets
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false); // pas de sites sponsorisés
user_pref("browser.search.suggest.enabled.private", true); // suggestion de recherche en navigation privée
user_pref("browser.tabs.closeWindowWithLastTab", false); // ne pas fermer le navigateur avec le dernier onglet
user_pref("browser.tabs.loadBookmarksInBackground", true); // ne pas changer d'onglet quand on clique de la molette sur un marque-page
user_pref("browser.toolbars.bookmarks.visibility", "always"); // barre des marques pages affichés sur toutes les pages
user_pref("browser.download.always_ask_before_handling_new_types", true); // demander s'il faut ouvrir ou enregistrer les fichiers
user_pref("browser.urlbar.placeholderName", "DuckDuckGo"); // moteur de recherche par défaut
user_pref("browser.urlbar.placeholderName.private", "DuckDuckGo"); // moteur de recherche par défaut en navigation privée
user_pref(
  "browser.search.hiddenOneOffs",
  "Google,Amazon.fr,Bing,DuckDuckGo,eBay,Qwant,Wikipédia (fr),LibRedirect"
); // cacher les boutons navigateurs de la barre URL lors d'une recherche
user_pref("browser.urlbar.shortcuts.bookmarks", false); // cacher le bouton marque-pages de la barre URL lors d'une recherche
user_pref("browser.urlbar.shortcuts.history", false); // cacher le bouton historique de la barre URL lors d'une recherche
user_pref("browser.urlbar.shortcuts.tabs", false); // cacher le bouton onglets de la barre URL lors d'une recherche
user_pref("browser.warnOnQuitShortcut", false); // ne pas alerter lorsque l'on quitte plusieurs onglets
user_pref("devtools.responsive.viewport.height", 760); // taille de l'inspecteur
user_pref("devtools.responsive.viewport.width", 340); // taille de l'inspecteur
user_pref("devtools.toolbox.host", "right"); // position de l'inspecteur
user_pref("dom.security.https_only_mode", true); // HTTPS only
user_pref("datareporting.healthreport.uploadEnabled", false); // télémétrie
user_pref("extensions.formautofill.creditCards.enabled", false); // pas d'auto-complétion de la carte de crédit
user_pref("media.eme.enabled", true); // lire le contenue protégé par des DRM
user_pref("findbar.highlightAll", true); // tout surligné lors d'une recherche
user_pref("pdfjs.sidebarViewOnLoad", 0); // ne pas affiché la table des matières par défaut dans les PDFs
user_pref("extensions.pocket.enabled", false); // désactive Pocket
user_pref("layout.css.has-selector.enabled", true); // https://developer.mozilla.org/en-US/docs/Web/CSS/:has
user_pref("extensions.webextensions.restrictedDomains", ""); // retire les domaines restreints (domanes de Mozilla) pour les extensions
user_pref("extensions.quarantinedDomains.enabled", false); // autorise les extensions à fonctionner sur tout les sites
user_pref("full-screen-api.warning.timeout", 0); // désactive le pop-up "site est désormais en plein écran"
user_pref("pdfjs.defaultZoomValue", "page-fit"); // change le zoom par défaut en "Page entière"
