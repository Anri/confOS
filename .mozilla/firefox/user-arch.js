user_pref("gfx.webrender.all", true); // Composition => WebRender (hardware)
user_pref("media.ffmpeg.vaapi.enabled", true); // VA-API (hardware acceleration)
user_pref("apz.overscroll.enabled", true); // Animation quand on scroll trop vers le haut/bas
