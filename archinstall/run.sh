#!/bin/sh

curl -O https://git.mylloon.fr/Anri/confOS/raw/branch/main/archinstall/user_configuration.json
curl -O https://git.mylloon.fr/Anri/confOS/raw/branch/main/archinstall/user_disk_layout.json

archinstall --config user_configuration.json --disk-layout user_disk_layout.json
