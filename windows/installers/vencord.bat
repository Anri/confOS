@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

ECHO Close Discord
TASKKILL /im Discord.exe /F

ECHO Force update Discord
ECHO Y | winget install --force -he Discord.Discord

SET "discord_dir=%LOCALAPPDATA%\Discord"
SET "vencord=%TEMP%\vencord.exe"

ECHO Download Vencord
PowerShell -Command "" ^
  "$link = 'https://github.com/Vencord/Installer/releases/latest/download/VencordInstallerCli.exe';" ^
  "Invoke-WebRequest -Uri $link -OutFile '!vencord!'"

ECHO Reclose Discord
TIMEOUT 10 /NOBREAK
TASKKILL /im Discord.exe /F

ECHO Install Vencord
PowerShell -Command "" ^
  "Start-Process -Wait -NoNewWindow '!vencord!' '-install', '-location !discord_dir!';" ^
  "Remove-Item -Force '!vencord!'"

IF NOT "%1"=="yes" (
  :: Delete newly created Discord shortcut
  DEL %HOMEPATH%\Desktop\Discord.lnk 2>NUL
  ie4uinit -show
  ECHO Remove the desktop shortcut

  :: Restart Discord
  PowerShell Start-Process "!discord_dir!\app-*\Discord.exe"

  PAUSE
)
EXIT /B
