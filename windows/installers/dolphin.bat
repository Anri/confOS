@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

ECHO Dolphin...

:: Find .7z extractor
SET "7ZIP=%ProgramFiles%\7-Zip\7z.exe"
SET "7ZIP2=%LOCALAPPDATA%\Microsoft\WindowsApps\7z.exe"
SET "NANAZIP=%LOCALAPPDATA%\Microsoft\WindowsApps\NanaZipC.exe"

IF EXIST "!7ZIP!" (
  SET "extractor=!7ZIP!"
)
IF NOT DEFINED extractor IF EXIST "!7ZIP2!" (
  SET "extractor=!7ZIP2!"
)
IF NOT DEFINED extractor IF EXIST "!NANAZIP!" (
  SET "extractor=!NANAZIP!"
)
IF NOT DEFINED extractor (
  ECHO No extractor for 7z files found.
  IF NOT "%1"=="yes" PAUSE
  EXIT /B 1
)

PowerShell -Command "" ^
  "$ie = New-Object -com internetexplorer.application;" ^
  "$ie.navigate('https://fr.dolphin-emu.org/download/');" ^
  "while ($ie.Busy -eq $true) { Start-Sleep -Milliseconds 1000 };" ^
  "$link = $ie.Document.getElementsByClassName('btn-info win')[0].getAttribute('href');" ^
  "$archive = '%TEMP%\dolphin.7z';" ^
  "Invoke-WebRequest -Uri $link -OutFile $archive;" ^
  "Remove-Item '%LOCALAPPDATA%\Dolphin' -Recurse -ErrorAction SilentlyContinue;" ^
  "& '!extractor!' x $archive -o'%TEMP%' -y;" ^
  "Move-Item -Path %TEMP%\Dolphin-x64 -Destination %LOCALAPPDATA%\Dolphin;" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\Dolphin.lnk');" ^
  "$Shortcut.TargetPath = '%LOCALAPPDATA%\Dolphin\Dolphin.exe';" ^
  "$Shortcut.Save();" ^
  "Remove-Item -Force $archive"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\dolphin" /f /v DisplayName /t REG_SZ /d "Dolphin"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\dolphin" /f /v InstallLocation /t REG_SZ /d "%LOCALAPPDATA%\Dolphin"

IF NOT "%1"=="yes" PAUSE
EXIT /B
