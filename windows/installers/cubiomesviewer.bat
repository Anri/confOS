@ECHO OFF

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

ECHO Cubiomes viewer...
MKDIR "%LOCALAPPDATA%\cubiomes-viewer" 2>NUL
PowerShell -Command "" ^
  "$exe = 'cubiomes-viewer-static-win.exe';" ^
  "$link = 'https://github.com/Cubitect/cubiomes-viewer/releases/latest/download/' + $exe;" ^
  "$outfile = Join-Path -Path '%LOCALAPPDATA%\cubiomes-viewer' -ChildPath $exe;" ^
  "Invoke-WebRequest -Uri $link -OutFile $outfile;" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\cubiomes-viewer.lnk');" ^
  "$Shortcut.TargetPath = $outfile;" ^
  "$Shortcut.Save()"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\cubiomes-viewer" /f /v DisplayName /t REG_SZ /d "cubiomes-viewer"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\cubiomes-viewer" /f /v InstallLocation /t REG_SZ /d "%LOCALAPPDATA%\cubiomes-viewer"

IF NOT "%1"=="yes" PAUSE
EXIT /B
