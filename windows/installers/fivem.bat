@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

SET "fivem=%LOCALAPPDATA%\FiveM"
MKDIR !fivem! 2>NUL

ECHO FiveM...
PowerShell -Command "" ^
  "$baselink = 'https://runtime.fivem.net/client';" ^
  "$exe = 'FiveM.exe';" ^
  "$outFile = Join-Path -Path '!fivem!' -ChildPath $exe;" ^
  "Invoke-WebRequest -Uri \"https://runtime.fivem.net/client/$exe\" -OutFile $outFile;" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\FiveM.lnk');" ^
  "$Shortcut.WorkingDirectory = '!fivem!';" ^
  "$Shortcut.TargetPath = $outFile;" ^
  "$Shortcut.Save()"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\fivem" /f /v DisplayName /t REG_SZ /d "FiveM"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\fivem" /f /v InstallLocation /t REG_SZ /d "%LOCALAPPDATA%\FiveM"

IF NOT "%1"=="yes" PAUSE
EXIT /B
