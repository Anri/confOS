@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

ECHO LosslessCut...

:: Find .7z extractor
SET "7ZIP=%ProgramFiles%\7-Zip\7z.exe"
SET "7ZIP2=%LOCALAPPDATA%\Microsoft\WindowsApps\7z.exe"
SET "NANAZIP=%LOCALAPPDATA%\Microsoft\WindowsApps\NanaZipC.exe"

IF EXIST "!7ZIP!" (
  SET "extractor=!7ZIP!"
)
IF NOT DEFINED extractor IF EXIST "!7ZIP2!" (
  SET "extractor=!7ZIP2!"
)
IF NOT DEFINED extractor IF EXIST "!NANAZIP!" (
  SET "extractor=!NANAZIP!"
)
IF NOT DEFINED extractor (
  ECHO No extractor for 7z files found.
  IF NOT "%1"=="yes" PAUSE
  EXIT /B 1
)

PowerShell -Command "" ^
  "$archive = 'LosslessCut-win-x64.7z';" ^
  "$link = 'https://github.com/mifi/lossless-cut/releases/latest/download/' + $archive;" ^
  "$outfile = Join-Path -Path $env:TEMP -ChildPath $archive;" ^
  "Invoke-WebRequest -Uri $link -OutFile $outfile;" ^
  "cd $env:TEMP;" ^
  "& '!extractor!' x $archive -o'%ProgramFiles%\LosslessCut' -y;" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\LosslessCut.lnk');" ^
  "$Shortcut.TargetPath = '%ProgramFiles%\LosslessCut\LosslessCut.exe';" ^
  "$Shortcut.Save();" ^
  "Remove-Item -Force $archive"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\LosslessCut" /f /v DisplayName /t REG_SZ /d "LosslessCut"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\LosslessCut" /f /v InstallLocation /t REG_SZ /d "%ProgramFiles%\LosslessCut"

IF NOT "%1"=="yes" PAUSE
EXIT /B
