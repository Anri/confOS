@ECHO OFF

ECHO Captvty...
PowerShell -Command "" ^
  "$ie = New-Object -com internetexplorer.application;" ^
  "$ie.navigate('https://v3.captvty.fr/');" ^
  "while ($ie.Busy -eq $true) { Start-Sleep -Milliseconds 1000 };" ^
  "$link = $ie.Document.getElementsByClassName('zip')[0].getAttribute('href');" ^
  "$archive = '%TEMP%\captvty.zip';" ^
  "Invoke-WebRequest -UserAgent 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko' -Uri $link -OutFile $archive;" ^
  "Remove-Item '%LOCALAPPDATA%\Captvty' -Recurse -ErrorAction SilentlyContinue;" ^
  "Expand-Archive -Path $archive -DestinationPath '%LOCALAPPDATA%\Captvty' -Force;" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\Captvty.lnk');" ^
  "$Shortcut.TargetPath = '%LOCALAPPDATA%\Captvty\Captvty.exe';" ^
  "$Shortcut.Save();" ^
  "Remove-Item -Force $archive"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\captvty" /f /v DisplayName /t REG_SZ /d "Captvty"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\captvty" /f /v InstallLocation /t REG_SZ /d "%LOCALAPPDATA%\Captvty"

IF NOT "%1"=="yes" PAUSE
EXIT /B
