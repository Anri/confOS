@ECHO OFF

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

ECHO Outer Wilds Mod Manager...
PowerShell -Command "" ^
  "$repo = 'ow-mods/ow-mod-man';" ^
  "$releases = \"https://api.github.com/repos/$repo/releases/latest\";" ^
  "$assets = (Invoke-WebRequest $releases | ConvertFrom-Json)[0].assets;" ^
  "$link = ($assets | Where-Object { $_.name -like '*_x64_en-US.msi' }).browser_download_url;" ^
  "$exe = 'Outer.Wilds.Mod.Manager-setup.msi';" ^
  "$outFile = Join-Path -Path $env:TEMP -ChildPath $exe;" ^
  "Invoke-WebRequest -Uri $link -OutFile $outFile;" ^
  "Start-Process -Wait $outFile '/quiet', '/passive', '/qn';" ^
  "Remove-Item -Force $outFile"

IF NOT "%1"=="yes" PAUSE
EXIT /B
