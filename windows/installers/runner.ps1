param (
  [string]$script
)


$fileName = "$script.bat"
$filePath = Join-Path -Path $env:TEMP -ChildPath $fileName

$baseUrl = "https://git.mylloon.fr/Anri/confOS/raw/branch/main"

if ($script -eq "windows11") {
  $url = "$baseUrl/$fileName"
}
else {
  $url = "$baseUrl/windows/installers/$fileName"
}


Write-Host "Downloading $fileName..."
Invoke-WebRequest -Uri $url -OutFile $filePath
Start-Process $filePath -NoNewWindow -Wait;

Remove-Item -Path $filePath
exit
