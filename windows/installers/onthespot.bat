@ECHO OFF

ECHO onthespot...
MKDIR "%LOCALAPPDATA%\onthespot" 2>NUL
PowerShell -Command "" ^
  "$repo = 'justin025/onthespot';" ^
  "$releases = \"https://api.github.com/repos/$repo/releases/latest\";" ^
  "$assets = (Invoke-WebRequest $releases | ConvertFrom-Json)[0].assets;" ^
  "$link = ($assets | Where-Object { $_.name -like '*-x86_64.exe' }).browser_download_url;" ^
  "$exe = 'OnTheSpot-x86_64.exe';" ^
  "$outfile = Join-Path -Path '%LOCALAPPDATA%\onthespot' -ChildPath $exe;" ^
  "Invoke-WebRequest -Uri $link -OutFile $outFile;" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\onthespot.lnk');" ^
  "$Shortcut.TargetPath = $outfile;" ^
  "$Shortcut.Save()"

REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\onthespot" /f /v DisplayName /t REG_SZ /d "onthespot"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\onthespot" /f /v InstallLocation /t REG_SZ /d "%LOCALAPPDATA%\onthespot"

IF NOT "%1"=="yes" PAUSE
EXIT /B
