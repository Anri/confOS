@ECHO OFF

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

ECHO NOWT...
PowerShell -Command "" ^
  "$exe = 'NOWT-Installer.exe';" ^
  "$link = 'https://github.com/pwall2222/NOWT/releases/latest/download/' + $exe;" ^
  "$file = Join-Path -Path '%TEMP%' -ChildPath $exe;" ^
  "Invoke-WebRequest -Uri $link -OutFile $file;" ^
  "Start-Process -Wait $file '/SUPPRESSMSGBOXES', '/VERYSILENT';" ^
  "Remove-Item -Force $file"

IF NOT "%1"=="yes" PAUSE
EXIT /B
