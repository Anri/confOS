@ECHO OFF

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

ECHO DSR...
PowerShell -Command "" ^
  "$releases = 'https://git.mylloon.fr/api/v1/repos/Anri/dsr/releases/latest';" ^
  "$link = (Invoke-WebRequest $releases | ConvertFrom-Json)[0].assets.browser_download_url;" ^
  "$archive = '%TEMP%\dsr.zip';" ^
  "Invoke-WebRequest -Uri $link -OutFile $archive;" ^
  "Remove-Item '%LOCALAPPDATA%\DSR' -Recurse -ErrorAction SilentlyContinue;" ^
  "Expand-Archive -Path $archive -DestinationPath '%LOCALAPPDATA%\DSR' -Force;" ^
  "Move-Item -Path '%LOCALAPPDATA%\DSR\dsr-win32-x64\*' -Destination '%LOCALAPPDATA%\DSR' -Force;" ^
  "Remove-Item '%LOCALAPPDATA%\DSR\dsr-win32-x64';" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\DSR.lnk');" ^
  "$Shortcut.TargetPath = '%LOCALAPPDATA%\DSR\dsr.exe';" ^
  "$Shortcut.Save();" ^
  "Remove-Item -Force $archive"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\dsr" /f /v DisplayName /t REG_SZ /d "DSR"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\dsr" /f /v InstallLocation /t REG_SZ /d "%LOCALAPPDATA%\DSR"

IF NOT "%1"=="yes" PAUSE
EXIT /B
