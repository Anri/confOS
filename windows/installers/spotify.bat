@ECHO OFF

:: Install latest version of spotify
ECHO Install latest Spotify version
ECHO Y | winget install -he Spotify.Spotify

:: Close spotify
PowerShell -Command "Stop-Process -Name Spotify -ErrorAction SilentlyContinue"

:: Patch spotify
ECHO Patch spotify with BlockTheSpot
PowerShell -Command "" ^
  "cd $env:TEMP;" ^
  "$urlElf = 'https://github.com/mrpond/BlockTheSpot/releases/latest/download/chrome_elf.zip';" ^
  "$destElf = Join-Path -Path $PWD -ChildPath 'chrome_elf.zip';" ^
  "$dir = Join-Path -Path $env:APPDATA -ChildPath 'Spotify';" ^
  "$exe = Join-Path -Path $dir -ChildPath 'Spotify.exe';" ^
  "Invoke-WebRequest -Uri $urlElf -OutFile $destElf;" ^
  "Expand-Archive -Force -LiteralPath $destElf -DestinationPath $PWD | Wait-Process;" ^
  "Remove-Item -LiteralPath $destElf -Force;" ^
  "$patchedFiles = (Join-Path -Path $PWD -ChildPath 'dpapi.dll')," ^
  "(Join-Path -Path $PWD -ChildPath 'config.ini');" ^
  "Copy-Item -LiteralPath $patchedFiles -Destination $dir;" ^
  "Remove-Item -LiteralPath $patchedFiles;" ^
  "Start-Process -WorkingDirectory $dir -FilePath $exe"

IF NOT "%1"=="yes" (
  :: Delete newly created Spotify shortcut
  DEL %HOMEPATH%\Desktop\Spotify.lnk 2>NUL
  ie4uinit -show
  ECHO Remove the desktop shortcut

  :: Restart Spotify
  PowerShell -Command "" ^
    "$dir = Join-Path -Path $env:APPDATA -ChildPath 'Spotify';" ^
    "$exe = Join-Path -Path $dir -ChildPath 'Spotify.exe';" ^
    "Start-Process -WorkingDirectory $dir -FilePath $exe"

  PAUSE
)
EXIT /B
