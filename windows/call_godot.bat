@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

FOR /F "usebackq delims=" %%i IN (`dir /B /S "*-stable_win64.exe"`) DO SET "godot=%%i"
PowerShell -Command "Start-Process cmd -Argument '/c START /B !godot!' -WindowStyle hidden"
EXIT /B
