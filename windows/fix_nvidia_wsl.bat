@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

ECHO Fix for /usr/lib/wsl/lib/libcuda.so.1

ECHO Enter the folder
CD \Windows\System32\lxss\lib

set "files=libcuda.so libcuda.so.1"

ECHO Remove existing symlinks
for %%i in (!files!) do (
  DEL %%i
)

:: Create new symlinks
for %%i in (!files!) do (
  MKLINK %%i libcuda.so.1.1
)

:: Update symlinks and rebuild the cache
bash -c "sudo ldconfig && echo 'Updated symbolic links and rebuilt cache'"

PAUSE
EXIT /B
