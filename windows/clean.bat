@ECHO OFF

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

ECHO CTMPF :: Clean TeMPorary Files

:: Empty the user's temp folder
ECHO Delete files in %TEMP%...
DEL /S /F /Q %LOCALAPPDATA%\Temp\* 2>NUL
RMDIR /S /Q "%TEMP%" 2>NUL

:: Empty the system's temp folder
ECHO Delete files in %WINDIR%\temp...
DEL /S /F /Q %WINDIR%\temp\* 2>NUL

:: Empty the Prefetch folder
ECHO Delete files in %WINDIR%\Prefetch...
RMDIR /S /Q "%WINDIR%\Prefetch" 2>NUL

:: Remove old Windows installation
if exist "%SYSTEMDRIVE%\Windows.old" (
  ECHO Delete old Windows installation in %SYSTEMDRIVE%\Windows.old...
  TAKEOWN /f "%SYSTEMDRIVE%\Windows.old" /a /r /d y
  ICACLS "%SYSTEMDRIVE%\Windows.old" /grant administrators:F /t
  RMDIR /S /Q "%SYSTEMDRIVE%\Windows.old"
)

:: Empty useless files of Windows
ECHO Delete files in C:\$WinREAgent...
RMDIR /S /Q "C:\$WinREAgent" 2>NUL
ECHO Delete files in C:\Recovery...
RMDIR /S /Q "C:\Recovery" 2>NUL

:: Empty AMD installer files
ECHO Delete files in C:\AMD...
RMDIR /S /Q "C:\AMD" 2>NUL

:: Empty Windows generated logs
ECHO Delete files in C:\PerfLogs...
RMDIR /S /Q "C:\PerfLogs" 2>NUL

:: Delete Dump
ECHO Delete stack files in C:\DumpStack.log...
DEL /Q "C:\DumpStack.log" 2>NUL

:: Delete .cache
ECHO Delete files in C:\.cache...
RMDIR /S /Q "C:\.cache" 2>NUL

:: Empty NVidia shader caches
ECHO Delete NVidia cache...
DEL /S /F /Q %LOCALAPPDATA%Low\NVIDIA\PerDriverVersion\DXCache\* 2>NUL
DEL /S /F /Q %LOCALAPPDATA%\NVIDIA\GLCache\* 2>NUL

:: Clean disks
ECHO Clean computer with the cleanmgr tool...
CD %WINDIR%\system32
cleanmgr /AUTOCLEAN
cleanmgr /VERYLOWDISK

EXIT /B
