@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

:: Exclusion list
SET ^"exclusions=^
  Spotify.Spotify"

:: Apply exclude list
for %%i in (!exclusions!) do (
  winget pin add --id %%i >NUL
)

:: Update Windows programs
winget upgrade -h --all

:: Clean list of exclusions
for %%i in (!exclusions!) do (
  winget pin remove --id %%i >NUL
)

:: MSStore apps
:: TODO

:: Windows Update
PowerShell -Command "Set-ExecutionPolicy Bypass -Scope Process;" ^
                    "Get-WindowsUpdate -AcceptAll -Install"

:: Global update
:: TODO: see https://git.mylloon.fr/Anri/confOS/issues/11
:: "%ProgramFiles%\Topgrade\topgrade.exe"

PAUSE
EXIT /B
