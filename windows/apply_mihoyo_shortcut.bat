@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

SET "collapse=%LOCALAPPDATA%\CollapseLauncher\current"
SET "exe=CollapseLauncher.exe"
SET "iconpath=Assets\Images\GameIcon"
SET "shortcutpath=%ProgramData%\Microsoft\Windows\Start Menu\Programs\Collapse Launcher Team"
SET "args=open --region \"Global\" --play --game"

:: Genshin shortcut
ECHO Genshin shortcut creation...
PowerShell -Command "" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('!shortcutpath!\Genshin Impact.lnk');" ^
  "$Shortcut.TargetPath = '!collapse!\!exe!';" ^
  "$Shortcut.Arguments = '!args! \"Genshin Impact\"';" ^
  "$Shortcut.IconLocation = '!collapse!\!iconpath!\icon-genshin.ico';" ^
  "$Shortcut.Save()"

:: Honkai shortcut
ECHO Honkai shortcut creation...
PowerShell -Command "" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('!shortcutpath!\Honkai Star Rail - HSR.lnk');" ^
  "$Shortcut.TargetPath = '!collapse!\!exe!';" ^
  "$Shortcut.Arguments = '!args! \"Honkai: Star Rail\"';" ^
  "$Shortcut.IconLocation = '!collapse!\!iconpath!\icon-starrail.ico';" ^
  "$Shortcut.Save()"

:: ZZZ shortcut
ECHO ZZZ shortcut creation...
PowerShell -Command "" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('!shortcutpath!\Zenless Zone Zero - ZZZ.lnk');" ^
  "$Shortcut.TargetPath = '!collapse!\!exe!';" ^
  "$Shortcut.Arguments = '!args! \"Zenless Zone Zero\"';" ^
  "$Shortcut.IconLocation = '!collapse!\!iconpath!\icon-zenless.ico';" ^
  "$Shortcut.Save()"

IF NOT "%1"=="yes" PAUSE
EXIT /B
