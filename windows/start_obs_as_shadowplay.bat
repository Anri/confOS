@ECHO OFF

:: Start as administrator
fltmc >NUL 2>&1 || (
  PowerShell Start -Verb RunAs '%0' 2>NUL || (
    EXIT 1
  )
  EXIT 0
)

:: Working directory
CD "%ProgramFiles%\obs-studio\bin\64bit"

:: Run OBS
START "OBS Shadowplay batch wrapper" ^
  obs64.exe --profile "ReplayBuffer" --scene "ReplayBuffer" ^
            --portable --minimize-to-tray --disable-missing-files-check ^
            --disable-shutdown-check --startreplaybuffer

EXIT /B
