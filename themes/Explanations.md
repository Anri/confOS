# User documentation

## How to use?

Use [pyoncord](https://github.com/pyoncord/pyoncord).

# Developer documentation

> [Some utils](https://github.com/nexpid/VendettaThemeUtil) and
> [some documentation](https://docs.riichi.tech/objects/semantic-colors)

## Connections

- `v1` refers to the OG Discord style
- `v2` refers to the new UI style, also called `TabsV2`

- **`v1`** `BACKGROUND_TERTIARY` : Background of guild list
- **`v1`** `BACKGROUND_ACCENT` : Background of invitation button
- **`v1`** `SPOILER_HIDDEN_BACKGROUND` : Spoiler when hidden
- **`v1`** `SPOILER_REVEALED_BACKGROUND` : Spoiler when revealed

---

- **`v1`** `PRIMARY_560` : Background of card in forums
- **`v1`** `PRIMARY_600` : Background app (starting screen, tchat)
- **`v1`** `PRIMARY_630` : Background channel list
- **`v1`** `PRIMARY_660` : Background button aside of chatbox
- **`v1`** `PRIMARY_700` : Background of chatbox
- **`v1`** `PRIMARY_800` : Backgroud of menubar and in profile

- **`v2`** `PLUM_15` : Background of search bar and invite button in guilds
- **`v2`** `PLUM_17` : Background of navbar
- **`v2`** `PLUM_18` : Background of buttons in "You" tab + settings
- **`v2`** `PLUM_19` : Background of cards used in mod settings
- **`v2`** `PLUM_20` : Background of channel list + tchat
- **`v2`** `PLUM_23` : Background of guild list
- **`v2`** `PLUM_24` : Background of search bar in settings

## [Themes+](https://vendetta.nexpid.xyz/themes-plus)

> [Documentation is here](https://github.com/nexpid/VendettaThemesPlus/wiki)

- `iconpack` : MD3
