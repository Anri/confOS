#!/usr/bin/env bash

set -o errexit  # crash the script when a command crash
set -o pipefail # same as above for piped command
set -o nounset  # crash when a variable doesnt exist

# TRACE=1 for debug
if [[ "${TRACE-0}" == "1" ]]; then
  set -o xtrace
fi

cd "$(dirname "$0")" # change script directory

function main {
  # === Variables ==
  local repo
  repo=$1


  # === Package manager ==
  sudo sed -i "s/#Color/Color/g" /etc/pacman.conf
  sudo sed -i "s/#ParallelDownloads = 5/ParallelDownloads = 3/g" /etc/pacman.conf


  # === Update Arch mirrors ==
  sudo pacman -S --noconfirm pacman-contrib
  curl -s "https://archlinux.org/mirrorlist/?country=FR&country=DE&protocol=https&use_mirror_status=on" | \
  sed -e "s/^#Server/Server/" -e "/^#/d" | sudo bash -c "rankmirrors -n 7 - > /etc/pacman.d/mirrorlist"


  # === Rustup ==
  # Init rustup
  rustup default stable
  # Dev tools
  rustup component add rust-analysis rls


  # === Rust ==
  # Add Cargo packages from Rust
  mkdir -p "$HOME"/.cargo/bin
  fish -c "fish_add_path $HOME/.cargo/bin ||:"
  # Install cargo packages
  cargo install cargo-update cargo-cache cargo-audit tealdeer


  # === Paru ==
  git clone https://aur.archlinux.org/paru.git
  cd paru || exit
  makepkg -si --noconfirm
  # Remove folder
  cd ..
  rm -rf paru ||:
  paru --gendb
  # Clean files used for compilation
  sudo sed -i "s/#CleanAfter/CleanAfter/g" /etc/paru.conf


  # === AUR packages ==
  paru -S --noconfirm \
    topgrade-bin bfg tex-fmt


  # === Fish ==
  # Remove motd
  fish -c "set -U fish_greeting"
  # Reversed search
  curl -sL https://git.io/fisher | fish -c "source && fisher install jorgebucaran/fisher"
  fish -c "fisher install jethrokuan/fzf"
  fish -c "fisher install jorgebucaran/autopair.fish"
  # SSH
  wget -q https://gitlab.com/kyb/fish_ssh_agent/raw/master/functions/fish_ssh_agent.fish -O "$HOME"/.config/fish/functions/fish_ssh_agent.fish
  mkdir -p "$HOME"/.ssh
  # Fish theme
  fish -c "fish_config theme choose 'Base16 Eighties' && yes | fish_config theme save"
  fish -c "set fish_color_comment 5c6773" # custom comment color
  # Fish prompt
  wget -q "${repo}"/.config/fish/functions/fish_prompt.fish -O "$HOME"/.config/fish/functions/fish_prompt.fish
  # EZA colors
  fish -c "set -Ux EXA_COLORS 'di=1;36:da=35'"
  # Add configuration
  wget -q "${repo}"/.config/fish/config.fish -O "$HOME"/.config/fish/config.fish
  # Add abbreviations
  wget -q "${repo}"/.config/fish/conf.d/abbr.fish -O "$HOME"/.config/fish/conf.d/abbr.fish
  # Add aliases
  wget -q "${repo}"/.config/fish/conf.d/alias.fish -O "$HOME"/.config/fish/conf.d/alias.fish
  # Add functions
  functions=("__fish_list_current_token" "catall" "latex-color" "makeaway"
             "remove-path" "ssh-export" "ssh-import" "ugc" "update-theme")
  for function in "${functions[@]}"
  do
    wget -q "${repo}"/.config/fish/functions/"${function}".fish -O "$HOME"/.config/fish/functions/"${function}".fish
  done


  # === Default editor ==
  fish -c "set -Ux EDITOR nano"


  # === Micro configuration ==
  mkdir -p "$HOME"/.config/micro
  wget -q --show-progress "${repo}"/.config/micro/settings.json -O "$HOME"/.config/micro/settings.json
  micro -plugin install detectindent


  # # === GL4D ==
  # # Download, build and installation
  # git clone https://github.com/noalien/GL4Dummies.git
  # cd GL4Dummies || exit
  # make -f Makefile.autotools
  # ./configure
  # make
  # sudo make install
  # cd ..
  # rm -rf GL4Dummies ||:
  # # Fix for shared libraries (https://stackoverflow.com/a/9395355)
  # sudo ldconfig
  # # Add to path
  # fish -c "set -Uax LD_LIBRARY_PATH /usr/local/lib"


  # === tldr ==
  fish -c "tldr --update"
  # Fish completion
  local url_tldr
  url_tldr=$(curl -sL https://api.github.com/repos/dbrgn/tealdeer/releases/latest \
    | grep 'browser_download_url": ".*/completions_fish"' \
    | cut --delimiter=":" --field=2,3 \
    | tr -d \" )
  wget -q --show-progress "${url_tldr:1}" -O completions_fish
  mv completions_fish ~/.config/fish/completions/tldr.fish


  # === Manual color ==
  fish -c "set -Ux MANPAGER \"sh -c 'col -bx | bat -l man -p'\""
  fish -c "set -Ux MANROFFOPT \"-c\""


  # === Bat config ==
  mkdir -p "$HOME"/.config/bat
  wget -q --show-progress "${repo}"/.config/bat/config -O "$HOME"/.config/bat/config


  # === Git ==
  wget -q "${repo}"/dotfiles/.gitconfig -O "$HOME"/.gitconfig


  # === Python ==
  # Add PIP packages from Python to the path
  mkdir -p "$HOME"/.local/bin
  fish -c "fish_add_path $HOME/.local/bin ||:"


  # === OCaml ==
  # Init Opam and install stuff
  echo | opam init
  fish -c "yes | opam init"
  opam install --confirm-level="unsafe-yes" \
    utop ocaml-lsp-server ocamlformat core menhir pprint sexplib \
    ppx_sexp_conv cohttp-lwt-unix dotenv twostep lwt_ssl alcotest ppx_deriving
  wget -q "${repo}"/dotfiles/.ocamlformat -O "$HOME"/.ocamlformat
  # Add Opam packages from OCaml
  mkdir -p "$HOME"/.opam/default/bin
  fish -c "fish_add_path $HOME/.opam/default/bin ||:"


  # === Clangd ==
  wget -q "${repo}"/dotfiles/.clang-format -O "$HOME"/.clang-format


  # === Perl ==
  # Add perl packages to path
  fish -c "fish_add_path /usr/bin/vendor_perl ||:"


  # === Emacs ==
  # mkdir -p "$HOME"/.emacs.d
  # wget -q ${repo}/.emacs.d/init.el -O "$HOME"/.emacs.d/init.el


  # === MIDI files ==
  {
    echo "soundfont /usr/share/soundfonts/FluidR3_GS.sf2"
    echo "soundfont /usr/share/soundfonts/FluidR3_GM.sf2"
  } | sudo tee -a /etc/timidity/timidity.cfg > /dev/null
  sudo mkdir -p /usr/share/sounds/sf2
  sudo ln -s /usr/share/soundfonts/*.sf2 /usr/share/sounds/sf2/


  # === Update NPM ==
  sudo npm i -g npm@latest


  # === Topgrade ==
  wget -q "${repo}"/.config/topgrade/topgrade.toml -O "$HOME"/.config/topgrade.toml
}

main "$@"
