#!/usr/bin/env bash

set -o errexit  # crash the script when a command crash
set -o pipefail # same as above for piped command
set -o nounset  # crash when a variable doesnt exist

# TRACE=1 for debug
if [[ "${TRACE-0}" == "1" ]]; then
  set -o xtrace
fi

cd "$(dirname "$0")" # change script directory

function main {
  # === Variables ==
  local repo apt_arg
  repo=https://git.mylloon.fr/Anri/confOS/raw/branch/main
  apt_arg='-y -o Dpkg::Options::="--force-confnew"'

  # === Update ==
  eval "apt-get update ${apt_arg}"


  # === Packages ==
  eval "apt-get install ${apt_arg} \
          fish eza tealdeer bat wget topgrade git make micro difftastic zip"


  # === Fish ==
  # Default shell
  chsh -s fish
  # Remove motd
  fish -c "set -U fish_greeting"
  # Fisher
  curl -sL https://git.io/fisher | fish -c "source && fisher install jorgebucaran/fisher"
  # Auto pair
  fish -c "fisher install jorgebucaran/autopair.fish"
  # Fish theme
  fish -c "fish_config theme choose 'Base16 Eighties' && yes | fish_config theme save"
  fish -c "set fish_color_comment 5c6773" # custom comment color
  # Fish prompt
  wget -q "${repo}"/.config/fish/functions/fish_prompt.fish -O "$HOME"/.config/fish/functions/fish_prompt.fish
  # EZA colors
  fish -c "set -Ux EXA_COLORS 'di=1;36:da=35'"
  # Add configuration
  wget -q "${repo}"/.config/fish/config.fish -O "$HOME"/.config/fish/config.fish
  # Add abbreviations
  wget -q "${repo}"/.config/fish/conf.d/abbr.fish -O "$HOME"/.config/fish/conf.d/abbr.fish
  # Add aliases
  wget -q "${repo}"/.config/fish/conf.d/alias.fish -O "$HOME"/.config/fish/conf.d/alias.fish


  # === Default editor ==
  fish -c "set -Ux EDITOR nano"


  # === tldr ==
  fish -c "tldr --update"
  # Fish completion
  local url_tldr
  url_tldr=$(curl -sL https://api.github.com/repos/dbrgn/tealdeer/releases/latest \
    | grep 'browser_download_url": ".*/completions_fish"' \
    | cut --delimiter=":" --field=2,3 \
    | tr -d \" )
  wget -q --show-progress "${url_tldr:1}" -O completions_fish
  mv completions_fish ~/.config/fish/completions/tldr.fish


  # === Git ==
  wget -q "${repo}"/dotfiles/.gitconfig -O "$HOME"/.gitconfig


  # === Topgrade ==
  wget -q "${repo}"/.config/topgrade/topgrade.toml -O "$HOME"/.config/topgrade.toml


  # === Fix ==
  #  Since Termux isn't used a lot, I don't plan to make it a real distro
  #  supported by thoses scripts, so I give the bare minimum support to it
  # Fix SSH not configured
  sed -i "/fish_ssh_agent/d" "$HOME"/.config/fish/config.fish
  # Fix topgrade abbr
  sed -i "/topgrade/d" "$HOME"/.config/fish/conf.d/abbr.fish
}

if type termux-info &> /dev/null;
then
  main "$@"
else
  echo "Ce script ne fonctionne que sur Termux." >&2
fi
