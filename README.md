# Configuration des OS

> Pour rechercher efficacement, ^F "`===`" recherche catégorie par catégorie.

## Windows 11

> Dans l'invite de commande `cmd`

```batch
PowerShell -Command "Invoke-Expression """"& { $(Invoke-WebRequest -UseBasicParsing 'https://git.mylloon.fr/Anri/confOS/raw/branch/main/windows/installers/runner.ps1') } -Script windows11"""""
```

### Mise à jour de certains programmes

> Dans l'invite de commande `cmd`

```batch
PowerShell -Command "Invoke-Expression """"& { $(Invoke-WebRequest -UseBasicParsing 'https://git.mylloon.fr/Anri/confOS/raw/branch/main/windows/installers/runner.ps1') } -Script script"""""
```

Avec le paramètre `script` le programme à (ré-)installer, la liste des programmes
[disponibles se trouvent ici](./windows/installers).

## WSL (Arch)

Tout d'abord, installer [ArchWSL](https://github.com/yuk7/ArchWSL/releases)
(.appx et [.cer](https://wsldl-pg.github.io/ArchW-docs/Install-Certificate/))

Ensuite, il faut définir un utilisateur :

```bash
>Arch.exe
[root@PC ~]# echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/wheel
[root@PC ~]# useradd -m -G wheel -s /bin/bash anri
[root@PC ~]# passwd anri
[root@PC ~]# exit

>Arch.exe config --default-user anri
```

<details><summary><a href="https://github.com/microsoft/WSL/issues/5548">Fix bug <code>ldconfig</code></a> :</summary>

```batch
CD \Windows\System32\lxss\lib
DEL libcuda.so
DEL libcuda.so.1
MKLINK libcuda.so libcuda.so.1.1
MKLINK libcuda.so.1 libcuda.so.1.1
```

> Après il faut peut-être redémarrer WSL et `sudo ldconfig`.

</details>

Commande pour lancer mon installation :

```bash
>Arch.exe
[anri@PC ~]# curl https://git.mylloon.fr/Anri/confOS/raw/branch/main/wsl.sh -s | bash
```

## Arch

<details><summary>Procédure dans l'image ISO</summary>

> ### Layout FR
>
> ```bash
> loadkeys fr
> ```
>
> <details>
> <summary>Connexion en WiFi</summary>
>
> > ### Récupération clé hashée
> >
> > ```bash
> > wpa_passhrase <SSID> <clé WIFI> > /var/lib/iwd/<SSID>.psk
> > ```
> >
> > ### Ajout entête
> >
> > ```bash
> > echo -e "[Security]\nPreSharedKey=" >> /var/lib/iwd/<SSID>.psk
> > ```
> >
> > ### Modification fichier pour ne garder que l'entête
> >
> > ```bash
> > nano /var/lib/iwd/<SSID>.psk
> > ```
> >
> > ### Connexion au Wifi
> >
> > ```bash
> > iwctl station wlan0 connect <SSID>
> > ```
>
> </details>
>
> ### Copié la configuration
>
> [`run.sh`](./archinstall/run.sh)
>
> ```bash
> curl -o run.sh -L lstu.fr/m_ai_r
> ```
>
> ### Lancé la configuration
>
> Rendre le script exécutable
>
> ```bash
> chmod +x run.sh
> ```
>
> Lancer le script
>
> ```bash
> ./run.sh
> ```
>
> Ne pas aller dans le `chroot` à la fin de l'installation et redémarrer le pc
>
> ```bash
> reboot
> ```

</details>

Commande pour lancer mon installation :

```bash
curl https://git.mylloon.fr/Anri/confOS/raw/branch/main/arch.sh -s | bash
```

## Android

Avec [UAD](https://github.com/0x192/universal-android-debloater), ma liste
d'applications est [uad_exported_selection.txt](./uad_exported_selection.txt).

<!-- Penser a bien désactiver les applications, et non les désinstallés. -->

[Ici](https://git.mylloon.fr/Anri/cat/wiki/Debloat-MIUI) un petit descriptif de
quelques applications que je désactive qui sont mauvaises.

### Termux

```bash
curl https://git.mylloon.fr/Anri/confOS/raw/branch/main/termux.sh -s | bash
```

<details><summary><h2>Anciens scripts</h2></summary>

Ils restent là au cas où, mais attention ils ne sont **pas à jour**.

### Manjaro

Commande pour lancer mon installation :

```bash
curl https://git.mylloon.fr/Anri/confOS/raw/branch/main/.old/manjaro.sh -s | bash
```

### Ubuntu

Commande pour lancer mon installation :

```bash
curl https://git.mylloon.fr/Anri/confOS/raw/branch/main/.old/ubuntu.sh -s | bash
```

### WSL (Ubuntu)

Commande pour lancer mon installation :

```bash
# Lors de l'installation il vous sera demandé de choisir
# une configuration mail, choisir : "No configuration"
curl https://git.mylloon.fr/Anri/confOS/raw/branch/main/.old/ubuntu_wsl.sh -s | sudo bash
```

</details>

# Infos

- Import/Export clef GPG :

  - Exporter clef publique : `gpg --output public.gpg --armor --export user@email`
  - Exporter clef privée : `gpg --output private.gpg --armor --export-secret-key user@email`
  - Importer clefs publiques : `gpg --import public.gpg`
  - Importer clefs privées : `gpg --allow-secret-key-import --import private.gpg`
  - Récupérer numéro de clef (`sec`) : `gpg --list-secret-keys --keyid-format LONG user@email`
    (dans `sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]` c'est `30F2B65B9246B6CA`)
  - Utiliser clef pour Git : `git config --global user.signingkey <numéro de la clé>`

- Git :
  - `git config --global user.email "kennel.anri@tutanota.com"`
  - `git config --global user.name "Mylloon"`

# Post install

Après avoir installé son PC, la configuration peut changer !

Lance `make` après avoir pull la dernière version de ce dépôt pour se synchroniser.

> Plus d'info dans le [Makefile](./Makefile).
