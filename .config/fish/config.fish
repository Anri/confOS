if not status --is-interactive
    exit
end

# SSH
fish_ssh_agent

# Opam integration
source $HOME/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true
