function makeaway --description "Run 'make' from another directory"
    # Get function name
    set current_name $(status current-function)

    # Check if no arguments were provided, or if the first argument is '--help'
    if test -z $argv[1] || test "$argv[1]" = --help || test "$argv[1]" = -h
        echo -e "Usage: $current_name <directory> [-- <target>]"
        echo -e "  $current_name [-h|--help] \t\t  - Show this help message"
        echo -e "  $current_name <directory> \t\t   - Run 'make' in the specified directory"
        echo -e "  $current_name <directory> -- <target> - Run 'make <target>' in the specified directory"
        return 0
    end

    # Check if directory exists
    set directory "$argv[1]"
    if not test -d "$directory"
        echo "$current_name: directory doesn't exists." 1>&2
        return 1
    end

    # Find the target
    set target ""
    if test (count $argv) -ge 3 && test "$argv[2]" = --
        # Target required
        set target $argv[3]
    else if test (count $argv) -ge 2 && test "$argv[2]" = --
        # User used "--" without target
        echo "$current_name: no target found." 1>&2
        return 1
    else if test (count $argv) -ge 2 && not test "$argv[2]" = --
        # User used "--" without target
        echo "$current_name: unknown argument `$argv[2]`." 1>&2
        return 1
    end

    # Make
    cd "$directory"
    make $target
    cd -
end
