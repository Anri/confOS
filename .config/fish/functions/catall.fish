function catall --description "Print recursively multiple files."
    # Declare our arguments
    argparse h/help n/noheader -- $argv

    # Get function name
    set current_name $(status current-function)

    # Check if not enough arguments provided, or help menu
    if set -ql _flag_help || test -z $argv[2]
        echo -e "Usage: $current_name <directory> exts ..."
        echo -e "  $current_name [-h|--help] \t\t - Show this help message"
        echo -e "  $current_name <directory> *.c \t - Cat all .C files"
        echo -e "  $current_name <directory> *.c *.h ... - Cat .C and .H files, etc."
        echo -e "Available options:"
        echo -e "  -n | --noheader \t\t - Don't show the filenames before print"
        return 0
    end

    # Define directory
    set directory $argv[1]

    # Define extensions
    set exts -name "$argv[2]"
    for ext in $argv[3..-1]
        set -a exts -o -name "$ext"
    end

    # Extra args for better printing
    if not set -ql _flag_noheader
        set args -exec echo -e "File: {}" \;
    end
    set -a args -exec cat {} \; -exec echo "" \;

    # Print files
    find $directory \( $exts \) $args
end
