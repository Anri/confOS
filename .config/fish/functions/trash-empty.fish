function trash-empty --description "Empty the windows recycle bin."
    # Get function name
    set current_name $(status current-function)

    # Empty trash
    powershell.exe -Command "" \
        "\$bin = (New-Object -ComObject Shell.Application).NameSpace(10);" \
        "\$bin.items() | ForEach {" \
        "   Write-Host 'Deleting:' \$_.Name;" \
        "   Remove-Item \$_.Path -Recurse -Force;" \
        "}"
end
