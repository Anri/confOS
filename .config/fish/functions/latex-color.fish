function latex-color --description "Get the LaTeX color from HEX code"
    # Declare our arguments
    argparse h/help -- $argv

    # Get function name
    set current_name $(status current-function)

    # Check if not enough arguments provided, or help menu
    if set -ql _flag_help || test -z $argv[1]
        echo -e "Usage: $current_name \"hexcode\""
        echo -e "  $current_name [-h|--help] - Show this help message"
        return 0
    end

    # Remove '#'
    set hex $argv[1]
    if string match -qr '^#' -- $hex
        set hex (string sub -s 2 $hex)
    end

    # Check if the hex code is valid
    if not string match -qir '^[0-9A-F]{3,8}$' $hex
       and not contains $(string length $hex) $(string split " " "3 4 6 8")
        echo "Error: Invalid HEX code" 1>&2
        return 1
    end

    # Expand 3 to 4
    if test (string length $hex) -eq 3
        set hex $(string join "" $hex "F")
    end

    # Expand 4 to 8
    if test (string length $hex) -eq 4
        set hex "$(string replace -ar '(.)' '$1$1' $hex)FF"
    end

    # Expand 6 to 8
    if test (string length $hex) -eq 6
        set hex $(string join "" $hex "FF")
    end

    # Convert hex to RGB
    set r (math "0x$(string sub -s 1 -l 2 $hex) / 255")
    set g (math "0x$(string sub -s 3 -l 2 $hex) / 255")
    set b (math "0x$(string sub -s 5 -l 2 $hex) / 255")
    set a (math "0x$(string sub -s 7 -l 2 $hex) / 255")

    set -x LC_NUMERIC C
    if test $a -eq 1
        printf "{%.2f, %.2f, %.2f}\n" $r $g $b
    else
        printf "{%.2f, %.2f, %.2f, %.2f}\n" $r $g $b $a
    end
end
