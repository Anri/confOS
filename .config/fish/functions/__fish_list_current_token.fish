# This function is typically bound to Alt-L, it is used to list the contents
# of the directory under the cursor.

# Temporary function as of now it's not bundled in the latest fish release
# https://github.com/fish-shell/fish-shell/commit/9158395d10cfa584a988ba093cf7a93a70b48578
function __fish_echo --description 'run the given command after the current commandline and redraw the prompt'
    set -l line (commandline --line)
    string >&2 repeat -N \n --count=(math (commandline | count) - $line + 1)
    $argv >&2
    string >&2 repeat -N \n --count=(math (count (fish_prompt)) - 1)
    string >&2 repeat -N \n --count=(math $line - 1)
    commandline -f repaint
end

function __fish_list_current_token -d "List contents of token under the cursor if it is a directory, otherwise list the contents of the current directory"
    set -l val (commandline -t | string replace -r '^~' "$HOME")
    set -l cmd
    if test -d $val
        set cmd eza $val
    else
        set -l dir (dirname -- $val)
        if test $dir != . -a -d $dir
            set cmd eza $dir
        else
            set cmd eza
        end
    end
    __fish_echo $cmd
end
