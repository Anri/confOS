function ssh-export --description "Export SSH keys and config to a zip archive."
    # Save the entry directory
    set entry_directory "$PWD"

    # Create the archive
    set output_file "$HOME/ssh_keys.zip"
    cd "$HOME/.ssh"
    zip "$output_file" * -x 'known_hosts*' -x environment

    # Returns to the entry directory
    cd "$entry_directory"

    echo "Exported at $output_file."
end
