function trash --description "Move file or directory to the windows recycle bin."
    # Get function name
    set current_name $(status current-function)

    # Check if no arguments were provided
    if test -z $argv[1]
        echo -e "Usage: $current_name path/to/element"
        return 0
    end

    set element $argv[1]

    # Check if element exists
    if not test -e $element
        echo "File or directory doesn't exists" 1>&2
        return 1
    end

    set temp_dir (powershell.exe -Command "Write-Host \$env:TEMP")
    set path (wslpath -w $element)

    # Move element to temporary windows directory
    cmd.exe /c "MKDIR $temp_dir 2>NUL" &> /dev/null
    powershell.exe -Command "Copy-Item \"$path\" -Destination \"$temp_dir\""

    # Switch between trashing a file or a directory
    set switch DeleteFile
    if test -d $element
        set switch DeleteDirectory
    end

    # Move element to the Windows trash
    powershell.exe -Command "" \
        "Add-Type -AssemblyName Microsoft.VisualBasic;" \
        "[Microsoft.VisualBasic.FileIO.FileSystem]::$switch('$temp_dir\\$element', 'OnlyErrorDialogs', 'SendToRecycleBin')"

    # Delete the WSL path
    rm -r "$element";
end
