function ssh-import --description "Import SSH keys and config from zip archive."
    # Declare our arguments
    argparse h/help f/force -- $argv
    or return

    # Get function name
    set current_name $(status current-function)

    # Check if no arguments were provided, or if the first argument is '--help'
    if test -z $argv[1] || set -ql _flag_help
        echo -e "Usage: $current_name <archive.zip>"
        echo -e "  $current_name [-h|--help] \t\t- Show this help message"
        echo -e "  $current_name <archive.zip> \t\t- Update the current .ssh folder with the archive content"
        echo -e "  $current_name [-f|--force] <archive.zip> - Erase **all** the current .ssh folder and replace it with the archive content"
        return 0
    end


    set archive_file "$argv[1]"
    if test (count $argv) -ge 2
        # Check if usage is respected
        echo "$current_name: too much arguments." 1>&2
        return 1
    else if not test -e "$archive_file"
        # Check if file exists
        echo "$current_name: file doesn't exists." 1>&2
        return 1
    else if not test $(string sub --start=-3 "$archive_file") = zip
        # Check if file have .zip extension
        echo "$current_name: file isn't a zip archive." 1>&2
        return 1
    end

    set ssh_directory "$HOME/.ssh"

    if set -ql _flag_force
        # Delete the .ssh directory
        find $ssh_directory -type f -not -name known_hosts -not -name environment -delete
    end

    # Update and overwrite the .ssh directory with the archive content
    # Using modification date, and new files are added
    # Files removed aren't tracked
    unzip -uo "$archive_file" -d $ssh_directory

    # Ask user if he want to delete the archive now that it has been imported
    while read --nchars 1 -l response --prompt-str="Wanna delete the archive? (y/n) "
        or return 1 # if the read was aborted with ctrl-c/ctrl-d
        switch $response
            case y Y
                rm $archive_file
                echo "$current_name: archive deleted."
                break
            case n N
                break
            case '*'
                continue
        end
    end
end
