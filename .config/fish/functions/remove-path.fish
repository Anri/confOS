function remove-path --description "Remove an element from the PATH"
  # See https://github.com/fish-shell/fish-shell/issues/8604
  if set -l index (contains -i "$argv" $fish_user_paths)
    set -e fish_user_paths[$index]
    echo "Removed $argv from the path"
  end
end
