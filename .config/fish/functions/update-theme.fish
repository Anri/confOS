function update-theme --description "Update the shell theme."
    # Declare our arguments
    argparse h/help -- $argv
    or return

    # Get function name
    set current_name $(status current-function)

    # Check if no arguments were provided, or if the first argument is '--help'
    if test -z $argv[1] || set -ql _flag_help
        echo -e "Usage: $current_name light|dark"
        echo -e "  $current_name [-h|--help] \t- Show this help message"
        echo -e "  $current_name light \t\t- Go to light mode"
        echo -e "  $current_name dark \t\t- Go to dark mode"
        return 0
    end

    set theme "$argv[1]"

    if test (count $argv) -ge 2
        # Check if usage is respected
        echo "$current_name: too much arguments." 1>&2
        return 1
    end

    switch $theme
      case "light"
        # Fish theme
        fish_config theme choose "Solarized Light" && yes | fish_config theme save

        # Micro editor
        sed -i "s/monokai/bubblegum/g" "$HOME"/.config/micro/settings.json

        # Bottom theme
        sed -i "s/#\[styles\]/[styles]/g" "$HOME"/.config/bottom/bottom.toml
        sed -i "s/#theme = \"default\"/theme = \"default-light\"/g" "$HOME"/.config/bottom/bottom.toml

        # Bat theme
        sed -i "s/Dracula/Solarized (light)/g" "$HOME"/.config/bat/config
      case "dark"
        # Fish theme
        fish_config theme choose "Base16 Eighties" && yes | fish_config theme save
        set fish_color_comment 5c6773 # custom comment color

        # Micro editor
        sed -i "s/bubblegum/monokai/g" "$HOME"/.config/micro/settings.json

        # Bottom theme
        sed -i "s/#\[styles\]/[styles]/g" "$HOME"/.config/bottom/bottom.toml
        sed -i "s/theme = \"default-light\"/#theme = \"default\"/g" "$HOME"/.config/bottom/bottom.toml

        # Bat theme
        sed -i "s/Solarized (light)/Dracula/g" "$HOME"/.config/bat/config
      case "*"
        echo "$current_name: unknown theme." 1>&2
        return 1
    end
end
