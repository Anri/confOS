function ugc --description "Update git fork."
    # Declare our arguments
    argparse h/help c/current -- $argv
    or return

    # Get function name
    set current_name $(status current-function)

    set upstream_remote upstream

    # Check if no arguments were provided, or if the first argument is '--help'
    if set -ql _flag_help
        echo -e "Usage: $current_name"
        echo -e "  $current_name [-h|--help]    - Show this help message"
        echo -e "  $current_name \t\t     - Update master branch from '$upstream_remote' remote"
        echo -e "  $current_name [-c|--current] - Update current branch from '$upstream_remote' remote"
        return 0
    end

    # Check if we are in a git repository
    git status >/dev/null
    if not test $status -eq 0
        echo "$current_name: not in a git repository" 1>&2
        return 1
    end

    # Check if upstream exists
    if not contains upstream (git remote)
        echo "$current_name: not '$upstream_remote' remote" 1>&2
        echo "$current_name: use: git remote add $upstream_remote <URL>" 1>&2
        return 1
    end

    # Check what is the default branch
    set origin_remote origin
    set default_branch (git remote show $origin_remote | sed -n '/HEAD/s/.*: //p')
    set current_branch (git rev-parse --abbrev-ref HEAD)

    if not set -ql _flag_current
        # Change to default branch
        git switch $default_branch
    end

    # Pull from the origin remote
    git pull $origin_remote $default_branch

    # Merge upstream
    git fetch $upstream_remote
    git pull $upstream_remote $default_branch
    git push $origin_remote $default_branch

    if not set -ql _flag_current
        # Return to the branch we were before if we changed
        git switch $current_branch
    end
end
