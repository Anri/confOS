alias pwd "pwd | tee /dev/tty | clip.exe"
alias wpwd "wslpath -w . | tee /dev/tty | clip.exe"
alias storrent "storrent &> /dev/null & disown"
