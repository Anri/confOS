#!/usr/bin/fish

abbr ... "cd ../.."
abbr .... "cd ../../.."
abbr ..... "cd ../../../.."
abbr ...... "cd ../../../../.."
abbr - "cd -"
abbr bigupdate "paru -Syu --noconfirm && pacman-clean && tldr --update && yes"
abbr cat "bat -pp"
abbr c command # Cancel abbreviation, alias of \
abbr cd.. "cd .."
abbr cl "cat src/**.* | sed '/^\s*\$/d' | wc -l" # Count lines of code without newlines
abbr cp "cp -rv"
abbr diff difft
abbr mgrep "grep -Iirnw 'text' ." # Find anything inside files of current directory
abbr mgcc "clear; gcc -Wall -Wextra -Wconversion -Wdouble-promotion -Wshadow -Wcast-align -Wstrict-prototypes -fanalyzer -fsanitize=undefined -fsanitize-undefined-trap-on-error -g3 main.c && ./*.out"
abbr mg++ "clear; g++ -Wall -Wextra -Wdouble-promotion -Wshadow -Wnon-virtual-dtor -pedantic -g3 -Wold-style-cast -Wsign-conversion main.cpp && ./*.out"
abbr mmake "make clean && clear; make && ./*.out"
abbr nano micro
abbr ocamlbuild "ocamlbuild -no-hygiene"
abbr opamupdate "opam update && opam upgrade -y"
abbr tgz "tar czf 'Archive.tar.gz'"
abbr vgcc "clear; gcc -Wall -Wextra -Wshadow -Wcast-align -Wstrict-prototypes -fanalyzer -fsanitize=undefined -g *.c && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./*.out"
abbr vg++ "clear; g++ -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -g -Wold-style-cast -Wsign-conversion *.cpp && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./*.out"
abbr vmake "make clean && clear; make debug && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./*.out"
abbr zip "zip -r 'archive.zip' dossier"
abbr mfind "find /* 2> /dev/null | grep -i ''" # Search for any filename in all disks
abbr sfind "find * -type f -name '*.sync-conflict*' -ok rm {} \;"
abbr grp "git remote prune origin"
abbr lfs "git lfs install; git lfs fetch; git lfs checkout"
abbr topgrade "topgrade; pacman-clean"
abbr run "for i in (seq 100); ./program; end"
abbr commit-undo "git reset --hard HEAD~1 && git push --force"
abbr mjava "find . -name '*.java' -print | xargs javac -d bin && java --enable-preview -cp bin App"
abbr , "clear; "
abbr s "echo \$status"
