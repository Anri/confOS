#!/usr/bin/fish

alias activate "source bin/activate.fish"
alias gc "git clone"
alias mirrors_update "sudo echo 'Updating...'; curl -s 'https://archlinux.org/mirrorlist/?country=FR&country=GB&protocol=https&use_mirror_status=on' | sed -e 's/^#Server/Server/' -e '/^#/d' | sudo bash -c 'rankmirrors -n 7 - > /etc/pacman.d/mirrorlist' && sudo pacman -Syy"
alias pacman-clean "sudo pacman -Rsn \$(pacman -Qqtd) --noconfirm 2>/dev/null; sudo paccache -rk1"
alias untgz "tar xvzf"
alias paru-rebuild "checkrebuild | awk '{print \$2}' | xargs -r paru -S --rebuild=all --noconfirm"
alias ssh-fix "sudo chown $USER:$(id -gn) $HOME/.ssh/* && \
               sudo chmod u=r,g=,o= $HOME/.ssh/*; \
               sudo chmod u=r,g=r,o=r $HOME/.ssh/*.pub; \
               sudo chmod u=r,g=r,o=r $HOME/.ssh/config; \
               sudo chmod u=rw,g=,o= $HOME/.ssh/environment 2>/dev/null; \
               sudo chmod u=rw,g=,o= $HOME/.ssh/known_hosts 2>/dev/null"
alias pacman-find "pacman -Ss"
alias pacman-list "pacman -Qqe"
alias paru-find "paru -Ss"
alias paru-list "paru -Qqme"
alias meteo "curl -s wttr.in?T&lang=fr | string match -v -r '^Suivez'"
