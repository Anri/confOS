# COLORED MANUALS
set -x MANPAGER "sh -c 'col -bx | batcat -l man -p'"

# SSH
fish_ssh_agent
