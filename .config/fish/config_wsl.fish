
# NAS
if timeout 0.4 ping -c1 192.168.1.200 > /dev/null
  set -l options metadata,uid=$(id -u),gid=$(id -g)
  sudo mount -t drvfs "Y:" /mnt/y -o $options 2> /dev/null &
  sudo mount -t drvfs "Z:" /mnt/z -o $options 2> /dev/null &
end

# Sometimes the screen is getting lost
if not test -e /tmp/.X11-unix/X0
  ln -sf /mnt/wslg/.X11-unix/X0 /tmp/.X11-unix/
end
