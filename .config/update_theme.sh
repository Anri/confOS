#!/usr/bin/env bash

set -o errexit  # crash the script when a command crash
set -o pipefail # same as above for piped command
set -o nounset  # crash when a variable doesnt exist

# TRACE=1 for debug
if [[ "${TRACE-0}" == "1" ]]; then
  set -o xtrace
fi

cd "$(dirname "$0")" # change script directory

main() {
  if [ $# -eq 0 ]; then
    echo "No arguments supplied"
  else
    local GT_default
    GT_default=$(gsettings get org.gnome.Terminal.ProfilesList default | tr -d \')
    case $1 in
      "sunset" )  # Go to dark mode
        # Terminal profile
        gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ \
                      visible-name "Dark"
        gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ \
                      foreground-color "rgb(211,208,200)"
        gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ \
                      background-color "rgb(45,45,45)"

        # Shell theme
        fish -c "update-theme dark"

        # Keyboard backlight
        busctl call --system org.freedesktop.UPower \
          /org/freedesktop/UPower/KbdBacklight \
          org.freedesktop.UPower.KbdBacklight SetBrightness 'i' 1
      ;;


      "sunrise" )  # Go to light mode
        # Terminal profile
        gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ \
                      visible-name "Light"
        gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ \
                      foreground-color "rgb(23,20,33)"
        gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:"${GT_default}"/ \
                      background-color "rgb(253,246,227)"

        # Shell theme
        fish -c "update-theme light"

        # Keyboard backlight
        busctl call --system org.freedesktop.UPower \
          /org/freedesktop/UPower/KbdBacklight \
          org.freedesktop.UPower.KbdBacklight SetBrightness 'i' 0
      ;;


      * )
        echo "Can't interpret given argument"
      ;;
    esac
  fi
}

main "$@"
