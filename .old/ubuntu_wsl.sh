#!/usr/bin/env bash

if grep "WSL" /proc/version > /dev/null;
then
    # === Add repositories ==
    apt-add-repository ppa:fish-shell/release-3 -y

    # === Update Ubuntu ==
    sed -i "s/focal/jammy/g" /etc/apt/sources.list
    sed -i "s/focal/jammy/g" /etc/apt/sources.list.d/*.list
    apt update
    apt dist-upgrade -y
    apt full-upgrade -y

    # === Remove SU password ==
    sed -i "26s#.*#%sudo   ALL=(ALL:ALL) NOPASSWD: ALL#" /etc/sudoers
    sed -i "44s#.*#%sudo   ALL=(ALL:ALL) NOPASSWD: ALL#" /etc/sudoers
    sed -i "50s#.*#%sudo   ALL=(ALL:ALL) NOPASSWD: ALL#" /etc/sudoers

    # === Install packages ==
    apt install -y git apt-transport-https curl wget build-essential valgrind fish fzf bat procps micro libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev libassimp-dev libfftw3-dev pkg-config automake make autoconf libtool perl-tk python3-pygments python3-venv python3-tk git-lfs zip unzip sed libssl-dev

    # === Remove useless packages
    apt purge -y needrestart # remove kernel update message (https://github.com/microsoft/WSL/issues/7054#issuecomment-864271333)
    apt autoremove -y

    # == Locales ==
    # !TODO!

    # === Fish ==
    # Remove motd
    fish -c "set -U fish_greeting"
    # Reversed search
    curl -sL https://git.io/fisher | fish -c "source && fisher install jorgebucaran/fisher"
    fish -c "fisher install jethrokuan/fzf"
    # SSH
    rm "$HOME"/.config/fish/functions/fish_ssh_agent.fish 2>/dev/null
    wget -q https://gitlab.com/kyb/fish_ssh_agent/raw/master/functions/fish_ssh_agent.fish -P "$HOME"/.config/fish/functions/
    mkdir "$HOME"/.ssh
    # Custom config.fish
    rm "$HOME"/.config/fish/config.fish 2>/dev/null
    wget -q https://git.kennel.ml/Anri/confOS/raw/branch/main/.config/fish/config_wsl.fish -O "$HOME"/.config/fish/config.fish
    # Custom prompt shell (based on https://github.com/fish-shell/fish-shell/blob/master/share/tools/web_config/themes/Base16%20Eighties.theme)
    # Modifications: Changed comment color
    rm "$HOME"/.config/fish/functions/fish_prompt.fish 2> /dev/null
    wget -q https://git.kennel.ml/Anri/confOS/raw/branch/main/.config/fish/functions/fish_prompt.fish -P "$HOME"/.config/fish/functions/
    fish -c "set -U fish_color_normal normal"
    fish -c "set -U fish_color_command 99cc99"
    fish -c "set -U fish_color_quote ffcc66"
    fish -c "set -U fish_color_redirection d3d0c8"
    fish -c "set -U fish_color_end cc99cc"
    fish -c "set -U fish_color_error f2777a"
    fish -c "set -U fish_color_param d3d0c8"
    fish -c "set -U fish_color_comment 5c6773"
    fish -c "set -U fish_color_match 6699cc"
    fish -c "set -U fish_color_selection white --bold --background=brblack"
    fish -c "set -U fish_color_search_match bryellow --background=brblack"
    fish -c "set -U fish_color_history_current --bold"
    fish -c "set -U fish_color_operator 6699cc"
    fish -c "set -U fish_color_escape 66cccc"
    fish -c "set -U fish_color_cwd green"
    fish -c "set -U fish_color_cwd_root red"
    fish -c "set -U fish_color_valid_path --underline"
    fish -c "set -U fish_color_autosuggestion 747369"
    fish -c "set -U fish_color_user brgreen"
    fish -c "set -U fish_color_host normal"
    fish -c "set -U fish_color_cancel -r"
    fish -c "set -U fish_pager_color_completion normal"
    fish -c "set -U fish_pager_color_description B3A06D yellow"
    fish -c "set -U fish_pager_color_prefix normal --bold --underline"
    fish -c "set -U fish_pager_color_progress brwhite --background=cyan"
    fish -c "set -U fish_pager_color_selected_background --background=brblack"
    fish -c "set -U fish_color_option d3d0c8"
    fish -c "set -U fish_color_keyword 99cc99"
    # Adding aliases
    fish -c "abbr ls 'exa --git --icons -gl'"
    fish -c "abbr cp 'cp -rv'"
    fish -c "abbr rm 'rm -rf'"
    fish -c "abbr gcc 'clear; gcc -Wall -Wextra -Wshadow -Wcast-align -Wstrict-prototypes -fanalyzer -fsanitize=undefined -g main.c && ./a.out; rm a.out 2> /dev/null'"
    fish -c "abbr vgcc 'clear; gcc -Wall -Wextra -Wshadow -Wcast-align -Wstrict-prototypes -fanalyzer -fsanitize=undefined -g main.c && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./a.out; rm a.out 2> /dev/null'"
    fish -c "abbr g++ 'clear; g++ -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -g -Wold-style-cast -Wsign-conversion main.cpp && ./a.out; rm a.out 2> /dev/null'"
    fish -c "abbr vg++ 'clear; g++ -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -g -Wold-style-cast -Wsign-conversion main.cpp && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./a.out; rm a.out 2> /dev/null'"
    fish -c "abbr make 'clear; make && ./main; make clean 2> /dev/null'"
    fish -c "abbr vmake 'clear; make && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./main; make clean 2> /dev/null'"
    fish -c "abbr activate 'source bin/activate.fish'"
    fish -c "abbr c 'command'"
    fish -c "abbr vs 'code .'"
    fish -c "abbr untgz 'tar -xvzf'"
    fish -c "abbr tgz 'tar czf \"Archive.tar.gz\"'"
    fish -c "abbr - 'cd -'"
    fish -c "abbr cd.. 'cd ..'"
    fish -c "abbr cat 'batcat'"
    fish -c "abbr nano 'micro'"
    fish -c "abbr bigupdate 'sudo apt update && sudo apt full-upgrade -y && sudo apt autoremove -y && tldr --update && yes | sdk update && yes | sdk upgrade java'"
    fish -c "abbr d 'explorer.exe .'"
    fish -c "abbr gc 'git clone'"
    fish -c "abbr tlmgr 'sudo \$(which tlmgr)'"
    fish -c "abbr cl 'cat src/**.* | sed \'/^\s*$/d\' | wc -l'"

    # === Micro ==
    cd /usr/local/bin || exit
    curl https://getmic.ro | GETMICRO_PLATFORM=linux64 bash
    cd - || exit
    git config --global core.editor "micro"
    set -Ux EDITOR micro
    wget -q --show-progress https://git.kennel.ml/Anri/confOS/raw/branch/main/.config/micro/settings.json -O "$HOME"/.config/micro/settings.json
    micro -plugin install detectindent

    # === GL4D ==
    # Download, build and installation
    git clone https://github.com/noalien/GL4Dummies.git
    cd GL4Dummies || exit
    make -f Makefile.autotools
    ./configure
    make
    make install
    cd ..
    rm -rf GL4Dummies
    # Fix for shared libraries (https://stackoverflow.com/a/9395355)
    ldconfig
    # Add to path
    fish -c "set -Ua LD_LIBRARY_PATH /usr/local/lib"

    # === Latex ==
    wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
    tar -xzf install-tl-unx.tar.gz
    rm ./*.tar.gz
    cd install-tl-* || exit
    echo "I" | ./install-tl
    cd ..
    rm -rf install-tl-*
    fish -c "fish_add_path /usr/local/texlive/*/bin/x86_64-linux"
    fish -c "set -Ua MANPATH /usr/local/texlive/*/texmf-dist/doc/man"
    fish -c "set -Ua INFOPATH /usr/local/texlive/*/texmf-dist/doc/info"

    # === Sign commits ==
    git config --global commit.gpgsign true

    # === Rust ==
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
    source "$HOME"/.cargo/env
    fish -c "set -Up fish_user_paths ~/.cargo/bin"
    # Dev tools
    rustup component add rust-analysis
    rustup component add rls

    # === Exa ==
    cargo install exa
    # EXA colors
    fish -c "set -Ux EXA_COLORS 'di=1;36:da=35'"

    # === tldr ==
    cargo install tealdeer
    tldr --update
    # Fish completion
    latest=$(wget -qO- https://api.github.com/repos/dbrgn/tealdeer/releases/latest)
    url=$(grep 'browser_download_url": ".*/completions_fish"' <<< "$latest" | awk '{ print substr ($0, 32 ) }' | awk '{ print substr( $0, 1, length($0)-1 ) }')
    wget -q --show-progress "$url" -O completions_fish
    mv completions_fish ~/.config/fish/completions/tldr.fish

    # === Java ==
    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    # Adding Fish support if fish is installed
    if [ -x "$HOME"/.config/fish ] ; then
        if [ ! -f "$HOME"/.config/fish/functions/fisher.fish ] ; then # install fisher if not already installed
            curl -sL https://git.io/fisher | fish -c 'source && fisher install jorgebucaran/fisher'
        fi
        fish -c 'fisher install reitzig/sdkman-for-fish'
    fi
    sdk install java 17.0.4-tem # https://whichjdk.com/#adoptium-eclipse-temurin
    yes | sdk upgrade java

    # === Manual color ==
    fish -c "set -Ux MANPAGER \"sh -c 'col -bx | batcat -l man -p'\""

    # === Shared folders ==
    mkdir /mnt/z && mkdir /mnt/y

    # === Git ==
    git config --global pull.rebase true
    git config --global init.defaultBranch main

    # === Path ==
    # Add PIP packages from Python to the path
    fish_add_path /home/anri/.local/bin

    echo -e "\nInstallation terminée !\nIl faut redémarrer WSL (dans Powershell = wsl --shutdown)."
    echo -e "---\nPense bien à paramétrer ton terminal, exemple avec Windows Terminal :"
    echo    "- Ligne de commande = wsl.exe -d Ubuntu fish"
    echo    "- Profils par défaut => Apparence :"
    echo    "   - Jeu de couleur = One Half Dark"
    echo    "   - Type de police = CaskaydiaCove Nerd Font (à télécharger ici : https://www.nerdfonts.com/font-downloads)"
else
    echo "Ce script ne fonctionne que sur WSL (Ubuntu)."
fi
