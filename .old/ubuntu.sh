#!/usr/bin/env bash

if grep "Ubuntu" /proc/version > /dev/null;
then
    sudo apt update

    which git &> /dev/null || sudo apt install git -y # install git if not already installed
    which curl &> /dev/null || sudo apt install apt-transport-https curl -y # install curl (along with apt-transport-https) if not already installed
    which wget &> /dev/null || sudo apt install wget -y # install wget if not already installed

    # === Upgrade packages ==
    sudo apt full-upgrade -y

    # === Basic dev ==
    sudo apt install build-essential valgrind -y

    # === Fish ==
    sudo apt-add-repository ppa:fish-shell/release-3 -y
    sudo apt update
    sudo apt install fish fzf bat procps exa micro -y
    sudo snap install micro --classic
    # Remove motd
    fish -c "set -U fish_greeting"
    # Reversed search
    curl -sL https://git.io/fisher | fish -c "source && fisher install jorgebucaran/fisher"
    fish -c "fisher install jethrokuan/fzf"
    # SSH
    rm "$HOME"/.config/fish/functions/fish_ssh_agent.fish 2>/dev/null
    wget -q https://gitlab.com/kyb/fish_ssh_agent/raw/master/functions/fish_ssh_agent.fish -P "$HOME"/.config/fish/functions/
    mkdir "$HOME"/.ssh
    # Custom config.fish
    rm "$HOME"/.config/fish/config.fish 2>/dev/null
    wget -q https://git.kennel.ml/Anri/confOS/raw/branch/main/.config/fish/config_ubuntu.fish -O "$HOME"/.config/fish/config.fish
    # Custom prompt shell (based on https://github.com/fish-shell/fish-shell/blob/master/share/tools/web_config/themes/Base16%20Eighties.theme)
    # Modifications: Changed comment color
    rm "$HOME"/.config/fish/functions/fish_prompt.fish 2> /dev/null
    wget -q https://git.kennel.ml/Anri/confOS/raw/branch/main/.config/fish/functions/fish_prompt.fish -P "$HOME"/.config/fish/functions/
    fish -c "set -U fish_color_normal normal"
    fish -c "set -U fish_color_command 99cc99"
    fish -c "set -U fish_color_quote ffcc66"
    fish -c "set -U fish_color_redirection d3d0c8"
    fish -c "set -U fish_color_end cc99cc"
    fish -c "set -U fish_color_error f2777a"
    fish -c "set -U fish_color_param d3d0c8"
    fish -c "set -U fish_color_comment 5c6773"
    fish -c "set -U fish_color_match 6699cc"
    fish -c "set -U fish_color_selection white --bold --background=brblack"
    fish -c "set -U fish_color_search_match bryellow --background=brblack"
    fish -c "set -U fish_color_history_current --bold"
    fish -c "set -U fish_color_operator 6699cc"
    fish -c "set -U fish_color_escape 66cccc"
    fish -c "set -U fish_color_cwd green"
    fish -c "set -U fish_color_cwd_root red"
    fish -c "set -U fish_color_valid_path --underline"
    fish -c "set -U fish_color_autosuggestion 747369"
    fish -c "set -U fish_color_user brgreen"
    fish -c "set -U fish_color_host normal"
    fish -c "set -U fish_color_cancel -r"
    fish -c "set -U fish_pager_color_completion normal"
    fish -c "set -U fish_pager_color_description B3A06D yellow"
    fish -c "set -U fish_pager_color_prefix normal --bold --underline"
    fish -c "set -U fish_pager_color_progress brwhite --background=cyan"
    fish -c "set -U fish_pager_color_selected_background --background=brblack"
    fish -c "set -U fish_color_option d3d0c8"
    fish -c "set -U fish_color_keyword 99cc99"
    # Adding aliases
    fish -c "abbr ls 'exa -gl'"
    fish -c "abbr cp 'cp -rv'"
    fish -c "abbr rm 'rm -rf'"
    fish -c "abbr gcc 'clear; gcc -Wall -Wextra -Wshadow -Wcast-align -Wstrict-prototypes -fanalyzer -g main.c && ./a.out; rm a.out 2> /dev/null'"
    fish -c "abbr vgcc 'clear; gcc -Wall -Wextra -Wshadow -Wcast-align -Wstrict-prototypes -fanalyzer -g main.c && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./a.out; rm a.out 2> /dev/null'"
    fish -c "abbr g++ 'clear; g++ -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -g -Wold-style-cast -Wsign-conversion main.cpp && ./a.out; rm a.out 2> /dev/null'"
    fish -c "abbr vg++ 'clear; g++ -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -g -Wold-style-cast -Wsign-conversion main.cpp && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./a.out; rm a.out 2> /dev/null'"
    fish -c "abbr make 'clear; make && ./main; make clean 2> /dev/null'"
    fish -c "abbr vmake 'clear; make && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./main; make clean 2> /dev/null'"
    fish -c "abbr activate 'source bin/activate.fish'"
    fish -c "abbr c 'command'"
    fish -c "abbr vs 'code .'"
    fish -c "abbr untgz 'tar -xvzf'"
    fish -c "abbr - 'cd -'"
    fish -c "abbr cat 'batcat'"
    fish -c "abbr nano 'micro'"
    fish -c "abbr bigupdate 'sudo apt update && sudo apt full-upgrade -y && sudo apt autoremove -y && sudo snap refresh && tldr --update && sdk update && echo \'y\' | sdk upgrade java'"
    fish -c "abbr d 'nautilus . -w &> /dev/null & disown'"
    fish -c "abbr spotify 'LD_PRELOAD=/usr/local/lib/spotify-adblock.so spotify &> /dev/null & disown'"
    # Remove fish icon
    if [ -f /usr/share/applications/fish.desktop ]
    then
        sudo mv /usr/share/applications/fish.desktop /usr/share/applications/fish.desktop.bak
    fi
    # Fish by default in terminal app
    profil=$(gsettings get org.gnome.Terminal.ProfilesList list)
    gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "use-custom-command" "true"
    gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "custom-command" "fish"
    # Default colors
    gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "use-theme-colors" "false"
    gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "foreground-color" "rgb(211,208,200)"
    gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profil:2:-2}/" "background-color" "rgb(45,45,45)"

    # === Brave instead of Firefox ==
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
    sudo apt update
    sudo apt install brave-browser -y
    sudo snap remove firefox && rm -r "$HOME"/snap/firefox

    # === GL4D ==
    # Installing depedencies
    sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev libassimp-dev libfftw3-dev pkg-config automake make autoconf libtool -y
    # Download and build
    git clone https://github.com/noalien/GL4Dummies.git
    cd GL4Dummies || exit
    make -f Makefile.autotools
    ./configure
    make
    # Installing it
    sudo make install
    # Removing folder
    cd ..
    rm -rf GL4Dummies
    # Fix for shared libraries (https://stackoverflow.com/a/9395355)
    sudo ldconfig
    # Add to path
    if [ -f "$HOME"/.bashrc ] ; then
        echo -e "\n# GL4Dummies\nexport PATH=/usr/local/bin:\$PATH\nexport LD_LIBRARY_PATH=/usr/local/lib:\$LD_LIBRARY_PATH\n" >> "$HOME"/.bashrc
    fi
    if [ -f "$HOME"/.config/fish/config.fish ] ; then
        echo -e "\n# GL4Dummies\nset -gx LD_LIBRARY_PATH /usr/local/lib \$LD_LIBRARY_PATH\n" >> "$HOME"/.config/fish/config.fish
    fi

    # === Latex ==
    sudo apt install texlive-full python3-pygments -y
    # Path where icons are saved
    pathicons='/usr/share/applications'
    apps=("display-im6.q16" "prerex" "vprerex" "texdoctk" "info")
    # We remove the Ubuntu desktop shortcut only if it already exists
    for app in "${apps[@]}"
    do
        if [ -f $pathicons/"$app".desktop ]
        then
            sudo mv $pathicons/"$app".desktop $pathicons/"$app".desktop.bak
        fi
    done

    # === Python stuff ==
    sudo apt install python3-venv python3-tk -y

    # === Sign commits ==
    git config --global commit.gpgsign true

    # === Rust ==
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
    source "$HOME"/.cargo/env
    fish -c "fish_add_path $HOME/.cargo/bin"
    # Dev tools
    rustup component add rust-analysis
    rustup component add rls

    # === tldr ==
    cargo install tealdeer
    tldr --update
    # Fish completion
    latest=$(wget -qO- https://api.github.com/repos/dbrgn/tealdeer/releases/latest)
    url=$(grep 'browser_download_url": ".*/completions_fish"' <<< "$latest" | awk '{ print substr ($0, 32 ) }' | awk '{ print substr( $0, 1, length($0)-1 ) }')
    wget -q --show-progress "$url" -O completions_fish
    mv completions_fish ~/.config/fish/completions/tldr.fish

    # === Git LFS ==
    sudo apt install git-lfs -y

    # === VSCode ==
    sudo snap install code --classic

    # === Spotify + no-ads mod ==
    # We make sure that Snap and flatpak versions are uninstalled
    sudo snap remove spotify 2> /dev/null
    sudo flatpak uninstall com.spotify.Client 2> /dev/null
    # We install the version from the official website
    echo "deb [trusted=yes] http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
    sudo apt update
    sudo apt install spotify-client -y
    # Install mod
    git clone https://github.com/abba23/spotify-adblock.git
    cd spotify-adblock || exit
    make
    # Install mod
    sudo make install
    # Remove folder
    cd ..
    rm -rf spotify-adblock
    # Change shortcut
    if [ -f /usr/share/applications/spotify.desktop ] ; then
        sudo sed -i "7s#.*#Exec=env LD_PRELOAD=/usr/local/lib/spotify-adblock.so spotify %U#" /usr/share/applications/spotify.desktop
    fi
    # Remove banner
    cd "$(dirname "$(realpath -L "$(which spotify)")")"/Apps/ || exit # absolute path to Spotify
    sudo bash -c "unzip -p xpui.spa xpui.js | sed 's/{adsEnabled:\!0}/{adsEnabled:false}/' > xpui.js"
    sudo zip --update xpui.spa xpui.js
    sudo rm xpui.js

    # === Syncthing ==
    sudo curl -s -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
    echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
    sudo apt update
    sudo apt install syncthing -y
    # Launch it at startup
    sudo systemctl enable syncthing@"$USER".service
    sudo systemctl start syncthing@"$USER".service
    # Remove desktop shortcut only if it already exists
    if [ -f /usr/share/applications/syncthing-start.desktop ]
    then
        sudo mv /usr/share/applications/syncthing-start.desktop /usr/share/applications/syncthing-start.desktop.bak
    fi

    # === Discord ==
    wget -q --show-progress "https://discord.com/api/download?platform=linux&format=deb" -O discord.deb && sudo apt install ./discord.deb -y && rm discord.deb

    # === Obsidian ==
    latest=$(wget -qO- https://api.github.com/repos/obsidianmd/obsidian-releases/releases/latest)
    url=$(grep 'browser_download_url": ".*.snap"' <<< "$latest" | awk '{ print substr ($0, 32 ) }' | awk '{ print substr( $0, 1, length($0)-1 ) }')
    wget -q --show-progress "$url" -O obsidian.snap
    sudo snap install --dangerous obsidian.snap
    rm obsidian.snap

    # === Flameshot ==
    sudo apt install flameshot -y
    # Custom configuration
    rm "$HOME"/.config/flameshot/flameshot.ini 2> /dev/null
    wget -q --show-progress https://git.kennel.ml/Anri/confOS/raw/branch/main/.config/flameshot/flameshot.ini -P "$HOME"/.config/flameshot/
    # Change keybindings
    gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot "[]"
    CUSTOM_KEYBINDINGS_LIST=$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings)
    if [[ $CUSTOM_KEYBINDINGS_LIST == "@as []" ]] # creating new list if not exists
    then
        CUSTOM_KEYBINDINGS_LIST="['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/']"
    else # if already existing, adding our new element
        CUSTOM_KEYBINDINGS_LIST="${CUSTOM_KEYBINDINGS_LIST::-1}, '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/']"
    fi
    gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "$CUSTOM_KEYBINDINGS_LIST"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/ name 'Flameshot' # set name
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/ command 'flameshot gui' # set command
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/flameshot-shortcut/ binding 'Print' # set key
    # Remove desktop shortcut only if it already exists
    if [ -f /usr/share/applications/org.flameshot.Flameshot.desktop ]
    then
        sudo mv /usr/share/applications/org.flameshot.Flameshot.desktop /usr/share/applications/org.flameshot.Flameshot.desktop.bak
    fi

    # === Parsec ==
    wget -q --show-progress https://builds.parsec.app/package/parsec-linux.deb -O parsec.deb
    sudo apt install ./parsec.deb -y
    rm parsec.deb

    # === Onlyoffice ==
    sudo snap install onlyoffice-desktopeditors

    # === Emoji picker ==
    curl https://raw.githubusercontent.com/GaZaTu/x11-emoji-picker/master/install.sh -s | sh -s -- -y
    # Remove icon
    if [ -f /usr/share/applications/x11-emoji-picker.desktop ]
    then
        sudo mv /usr/share/applications/x11-emoji-picker.desktop /usr/share/applications/x11-emoji-picker.desktop.bak
    fi
    # Custom configuration
    rm "$HOME"/.config/gazatu.xyz/emoji-picker.ini 2>/dev/null
    wget -q --show-progress https://git.kennel.ml/Anri/confOS/raw/branch/main/.config/gazatu.xyz/emoji-picker.ini -P "$HOME"/.config/gazatu.xyz/

    # === Gnome configuration ==
    # Nautilus keybinding
    CUSTOM_KEYBINDINGS_LIST=$(gsettings get org.gnome.settings-daemon.plugins.media-keys custom-keybindings)
    if [[ $CUSTOM_KEYBINDINGS_LIST == "@as []" ]] # creating new list if not exists
    then
        CUSTOM_KEYBINDINGS_LIST="['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/']"
    else # if already existing, adding our new element
        CUSTOM_KEYBINDINGS_LIST="${CUSTOM_KEYBINDINGS_LIST::-1}, '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/']"
    fi
    gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "$CUSTOM_KEYBINDINGS_LIST" # updating the list
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/ name 'Nautilus' # set name
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/ command 'nautilus -w' # set command
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/nautilus-shortcut/ binding '<Super>E' # set shortcut
    # Remove the Trash icon from the bar
    gsettings set org.gnome.shell.extensions.dash-to-dock show-trash false
    # Move the Menu Button to the top of the bar
    gsettings set org.gnome.shell.extensions.dash-to-dock show-apps-at-top true
    # Move the taskbar to the bottom
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
    # Add minimized window on dock click
    gsettings set org.gnome.shell.extensions.dash-to-dock click-action minimize
    # Show battery pourcentage
    gsettings set org.gnome.desktop.interface show-battery-percentage true
    # Change size of icon in the Dock
    gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 29
    # Dark theme
    gsettings set org.gnome.desktop.interface gtk-theme "Yaru-dark"

    # === Unused icons ==
    pathicons='/usr/share/applications'
    apps=("debian-xterm" "debian-uxterm" "software-properties-drivers" "software-properties-livepatch")
    # Remove desktop shortcut only if it already exists
    for app in "${apps[@]}"
    do
        if [ -f $pathicons/"$app".desktop ]
        then
            sudo mv $pathicons/"$app".desktop $pathicons/"$app".desktop.bak
        fi
    done

    # === Clipboard history ==
    sudo apt install gnome-shell-extension-gpaste gnome-shell-extension-prefs -y
    gnome-extensions enable GPaste@gnome-shell-extensions.gnome.org

    # === Signal ==
    wget -O- -q --show-progress https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
    sudo cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
    rm signal-desktop-keyring.gpg
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
    # Install it
    sudo apt install signal-desktop -y

    # === Paint ==
    sudo snap install kolourpaint
    # Rename icon
    if [ -f /var/lib/snapd/desktop/applications/kolourpaint_kolourpaint.desktop ] ; then
        sudo sed -i "3s#.*#Name=Paint#" /var/lib/snapd/desktop/applications/kolourpaint_kolourpaint.desktop
    fi

    # === Java ==
    sudo apt install zip unzip sed -y # sed probably installed but it doesn't cost anything to double-check
    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    # Adding Fish support if fish is installed
    if [ -x "$HOME"/.config/fish ] ; then
        if [ ! -f "$HOME"/.config/fish/functions/fisher.fish ] ; then # install fisher if not already installed
            curl -sL https://git.io/fisher | fish -c 'source && fisher install jorgebucaran/fisher'
        fi
        fish -c 'fisher install reitzig/sdkman-for-fish'
    fi
    sdk install java 17.0.2-tem # https://whichjdk.com/#adoptium-eclipse-temurin
    echo "y" | sdk upgrade java

    # === MultiMC ==
    # latest=$(wget -qO- https://multimc.org/ | grep "Debian/Ubuntu") # retrieving for the official website
    # pat='href="(.*)" ' # regex
    # [[ $latest =~ $pat ]] # Use of REGEX
    # wget -q --show-progress -i "${BASH_REMATCH[1]}" -O MultiMC.deb
    # sudo apt install ./MultiMC.deb -y
    # rm MultiMC.deb
    # # Rename icon
    # if [ -f /usr/share/applications/multimc.desktop ] ; then
    #     sudo sed -i "7s#.*#Name=Minecraft#" /usr/share/applications/multimc.desktop
    # fi

    # === OBS ==
    # sudo add-apt-repository ppa:obsproject/obs-studio -y
    # sudo apt update
    # sudo apt install ffmpeg obs-studio -y

    # === NodeJS ==
    curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
    sudo apt-get install -y nodejs

    echo -e "\nInstallation terminée.\nIl faut redémarrer l'ordinateur."
else
    echo "Ce script ne fonctionne que sur Ubuntu."
fi
