@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "repo=https://git.mylloon.fr/Anri/confOS/raw/branch/main"
SET "local=%HOMEPATH%\Documents\Local"
SET "startmenu=%APPDATA%\Microsoft\Windows\Start Menu\Programs"

MKDIR "!local!" 2>NUL

:: => NO-ADMIN RIGHTS <=

fltmc >NUL 2>&1 || (
  ECHO Please wait until the administrator popup appears.

  :: === Update winget to latest version ==
  PowerShell -Command "" ^
    "Invoke-WebRequest -Uri https://aka.ms/getwinget -OutFile winget.msixbundle;" ^
    "Add-AppPackage -ForceApplicationShutdown .\winget.msixbundle;" ^
    "del .\winget.msixbundle"


  :: === Scripts from ./windows directory ==
  PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/update.bat -OutFile '!local!\update.bat'"
  PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/clean.bat -OutFile '!local!\ctmpf.bat'"
  PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/installers/vencord.bat -OutFile '!local!\vencord_installer.bat'"
  @REM PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/fix_nvidia_wsl.bat -OutFile '!local!\fix_nvidia_wsl.bat'"
  PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/start_obs_as_shadowplay.bat -OutFile '%LOCALAPPDATA%\start_obs_as_shadowplay.bat'"
  PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/reg_fixes.bat -OutFile '!local!\reg_fixes.bat'"


  :: Start as administrator
  PowerShell Start -Verb RunAs "%0 noskipadmin" 2>NUL || (
    EXIT 1
  )
  EXIT 0
)


:: => ADMIN RIGHTS <=

IF NOT "%1"=="noskipadmin" (
  ECHO You have to run the script as normal user first.

  PAUSE
  EXIT 0
)

:: === Registery modifications ==
CMD /c "!local!\reg_fixes.bat" yes


:: === WinGet ==
:: Auto accept ToT
ECHO Y | winget list >NUL
SET "pmi=winget install --silent --exact"

:: C++ libraries
SET ^"cpp_libs=^
  Microsoft.VCRedist.2005.x86 Microsoft.VCRedist.2005.x64 ^
  Microsoft.VCRedist.2008.x86 Microsoft.VCRedist.2008.x64 ^
  Microsoft.VCRedist.2010.x86 Microsoft.VCRedist.2010.x64 ^
  Microsoft.VCRedist.2012.x86 Microsoft.VCRedist.2012.x64 ^
  Microsoft.VCRedist.2013.x86 Microsoft.VCRedist.2013.x64 ^
  Microsoft.VCRedist.2015+.x86 Microsoft.VCRedist.2015+.x64"

ECHO C++ libraries...
for %%i in (!cpp_libs!) do (
  !pmi! %%i
)

:: .NET libraries
SET ^"net_libs=^
  Microsoft.DotNet.DesktopRuntime.3_1 Microsoft.DotNet.SDK.3_1 ^
  Microsoft.DotNet.DesktopRuntime.5 Microsoft.DotNet.SDK.5 ^
  Microsoft.DotNet.DesktopRuntime.6 Microsoft.DotNet.SDK.6 ^
  Microsoft.DotNet.DesktopRuntime.7 Microsoft.DotNet.SDK.7 ^
  Microsoft.DotNet.DesktopRuntime.8 Microsoft.DotNet.SDK.8"

ECHO .NET libraries...
for %%i in (!net_libs!) do (
  !pmi! %%i
)

:: Applications
SET ^"apps=^
  Mozilla.Firefox M2Team.NanaZip Parsec.Parsec sylikc.JPEGView KDE.Kdenlive ^
  voidtools.Everything.Lite CodecGuide.K-LiteCodecPack.Standard GIMP.GIMP ^
  Microsoft.VisualStudioCode Bitwarden.Bitwarden HandBrake.HandBrake ^
  TheDocumentFoundation.LibreOffice Armin2208.WindowsAutoNightMode ^
  OBSProject.OBSStudio Xournal++.Xournal++ Microsoft.PowerToys KDE.KDEConnect ^
  OpenWhisperSystems.Signal TenacityTeam.Tenacity Microsoft.PowerShell ^
  viarotel.Escrcpy Obsidian.Obsidian GeekUninstaller.GeekUninstaller ^
  Vendicated.Vencord GodotEngine.GodotEngine th-ch.YouTubeMusic KDE.Filelight"

ECHO Apps...
for %%i in (!apps!) do (
  !pmi! %%i
)

SET ^"games=^
  RiotGames.LeagueOfLegends.EUW RiotGames.Valorant.EU Valve.Steam ppy.osu ^
  HeroicGamesLauncher.HeroicGamesLauncher PrismLauncher.PrismLauncher ^
  pizzaboxer.Bloxstrap CollapseLauncher.Collapse molenzwiebel.Deceive ^
  ebkr.r2modman"

ECHO Games...
for %%i in (!games!) do (
  !pmi! %%i
)


:: === Install WSL ==
ECHO WSL...
wsl --install --no-distribution


:: === Activate Windows ==
:: https://massgrave.dev/command_line_switches#uses-in-powershell-one-liner
PowerShell -Command "" ^
  "& ([ScriptBlock]::Create((irm https://get.activated.win )))" ^
  "/HWID"


:: === Modules for Windows Update ==
PowerShell -Command "Install-PackageProvider -Name NuGet -Force"
PowerShell -Command "Install-Module -Name PSWindowsUpdate -Force"


:: === Geek Uninstaller icon ==
ECHO Geek Uninstaller icon...
PowerShell -Command "" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\Geek Uninstaller.lnk');" ^
  "$Shortcut.TargetPath = '%LOCALAPPDATA%\Microsoft\WinGet\Packages\GeekUninstaller.GeekUninstaller_Microsoft.Winget.Source_8wekyb3d8bbwe\geek.exe';" ^
  "$Shortcut.Save()"

:: === Godot Engine icon ==
ECHO Godot Engine icon...
PowerShell -Command "" ^
  "$loc = '%LOCALAPPDATA%\Microsoft\WinGet\Packages\GodotEngine.GodotEngine_Microsoft.Winget.Source_8wekyb3d8bbwe';" ^
  "$ico = 'https://raw.githubusercontent.com/godotengine/godot/refs/heads/master/platform/windows/godot.ico';" ^
  "Invoke-WebRequest -Uri !repo!/windows/call_godot.bat -OutFile \"$loc\call.bat\";" ^
  "Invoke-WebRequest -Uri \"$ico\" -OutFile \"$loc\icon.ico\";" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('%APPDATA%\Microsoft\Windows\Start Menu\Programs\Godot Engine.lnk');" ^
  "$Shortcut.TargetPath = \"$loc\call.bat\";" ^
  "$Shortcut.WorkingDirectory = \"$loc\";" ^
  "$Shortcut.IconLocation = \"$loc\icon.ico\";" ^
  "$Shortcut.Save()"


:: === LosslessCut ==
PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/installers/losslesscut.bat -OutFile '%TEMP%\losslesscut.bat'"
CMD /c "%TEMP%\losslesscut.bat" yes


:: === DSR ==
PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/installers/dsr.bat -OutFile '%TEMP%\dsr.bat'"
CMD /c "%TEMP%\dsr.bat" yes


:: === OpenTabletDriver ==
ECHO OpenTabletDriver...
PowerShell -Command "" ^
  "$archive = '%TEMP%\OpenTabletDriver.win-x64.zip';" ^
  "Invoke-WebRequest -Uri https://github.com/OpenTabletDriver/OpenTabletDriver/releases/latest/download/OpenTabletDriver.win-x64.zip -OutFile $archive;" ^
  "Expand-Archive -Path $archive -DestinationPath '%ProgramFiles%\OpenTabletDriver' -Force;" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('!startmenu!\OpenTabletDriver.lnk');" ^
  "$Shortcut.TargetPath = '%ProgramFiles%\OpenTabletDriver\OpenTabletDriver.UX.Wpf.exe';" ^
  "$Shortcut.WorkingDirectory = '%ProgramFiles%\OpenTabletDriver';" ^
  "$Shortcut.Save();" ^
  "Remove-Item -Force $archive"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\OpenTabletDriver" /f /v DisplayName /t REG_SZ /d "OpenTabletDriver"
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Uninstall\OpenTabletDriver" /f /v InstallLocation /t REG_SZ /d "%ProgramFiles%\OpenTabletDriver"

:: WindowsInk for OpenTabletDriver
ECHO WindowsInk support for OpenTabletDriver...
PowerShell -Command "Invoke-WebRequest -Uri https://github.com/X9VoiD/vmulti-bin/releases/download/v1.0/VMulti.Driver.zip -OutFile '%TEMP%\VMulti-Driver.zip'"
PowerShell -Command "Expand-Archive -Path '%TEMP%\VMulti-Driver.zip' -DestinationPath '%TEMP%\VMulti-Driver' -Force"
CALL "%TEMP%\VMulti-Driver\install_hiddriver.bat"
PowerShell -Command "Invoke-WebRequest -Uri https://github.com/X9VoiD/VoiDPlugins/releases/latest/download/WindowsInk.zip -OutFile '%TEMP%\WindowsInk.zip'"
PowerShell -Command "Expand-Archive -Path '%TEMP%\WindowsInk.zip' -DestinationPath '%LOCALAPPDATA%\OpenTabletDriver\Plugins\Windows Ink' -Force"

:: Presets of OpenTabletDriver
ECHO Downloading OpenTabletDriver presets...
MKDIR "%LOCALAPPDATA%\OpenTabletDriver\Presets" 2>NUL
PowerShell -Command "Invoke-WebRequest -Uri !repo!/opentabletdriver/vertical-main.json -OutFile '%LOCALAPPDATA%\OpenTabletDriver\Presets\vertical-main.json'"
PowerShell -Command "Invoke-WebRequest -Uri !repo!/opentabletdriver/vertical-second.json -OutFile '%LOCALAPPDATA%\OpenTabletDriver\Presets\vertical-second.json'"

:: Settings of OpenTabletDriver, using 'vertical-main.json' as default settings
ECHO Setting up OpenTabletDriver...
XCOPY "%LOCALAPPDATA%\OpenTabletDriver\Presets\vertical-main.json" "%LOCALAPPDATA%\OpenTabletDriver\settings.json" /-I /Y

:: Start minimized at startup
PowerShell -Command "" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('!startmenu!\Startup\OpenTabletDriver.lnk');" ^
  "$Shortcut.TargetPath = '%ProgramFiles%\OpenTabletDriver\OpenTabletDriver.UX.Wpf.exe';" ^
  "$Shortcut.WorkingDirectory = '%ProgramFiles%\OpenTabletDriver';" ^
  "$Shortcut.WindowStyle = 7;" ^
  "$Shortcut.Save()"


:: === NOWT ==
PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/installers/nowt.bat -OutFile '%TEMP%\nowt.bat'"
CMD /c "%TEMP%\nowt.bat" yes


:: === Minecraft ==
:: Copy shortcut of Prism Launcher to a Minecraft shortcut
COPY /Y "!startmenu!\Prism Launcher.lnk" "!startmenu!\Minecraft.lnk"


:: === Cubiomes viewer ==
PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/installers/cubiomesviewer.bat -OutFile '%TEMP%\cubiomesviewer.bat'"
CMD /c "%TEMP%\cubiomesviewer.bat" yes


:: === Firefox configuration ==
:: Open and close Firefox for the first time, generating a profile
"%ProgramFiles%\Mozilla Firefox\firefox.exe"
TIMEOUT 5
TASKKILL /im firefox.exe /F

ECHO Firefox configuration...
FOR /F "usebackq delims=" %%i IN (`dir /B /S "%APPDATA%\Mozilla\Firefox\Profiles\*.default-release"`) DO SET "vbc=%%i"
PowerShell -Command "Invoke-WebRequest -Uri !repo!/.mozilla/firefox/user.js -OutFile '!vbc!\user.js'"

:: Download addons settings in "Download" folder
PowerShell -Command "Invoke-WebRequest -Uri !repo!/.mozilla/firefox/extensions/SponsorBlockConfig.json -OutFile '%HOMEPATH%\Downloads\SponsorBlockConfig.json'"
PowerShell -Command "Invoke-WebRequest -Uri !repo!/.mozilla/firefox/extensions/auto-tab-discard-preferences.json -OutFile '%HOMEPATH%\Downloads\auto-tab-discard-preferences.json'"
ECHO Configuration files for Firefox Addons are in the download folder (%HOMEPATH%\Downloads).


:: === WindowsAutoNightMode ==
ECHO WindowsAutoNightMode configuration...
MKDIR "%APPDATA%\AutoDarkMode" 2>NUL
PowerShell -Command "Invoke-WebRequest -Uri !repo!/.config/WindowsAutoNightMode/config.yaml -OutFile '%APPDATA%\AutoDarkMode\config.yaml'"
"%LOCALAPPDATA%\Programs\AutoDarkMode\adm-app\AutoDarkModeApp.exe"


:: === OBS ==
:: Create directories for configurations
MKDIR "%ProgramFiles%\obs-studio\config\obs-studio\basic\profiles\ReplayBuffer" 2>NUL
MKDIR "%ProgramFiles%\obs-studio\config\obs-studio\basic\scenes" 2>NUL

:: Download configuration
PowerShell -Command "Invoke-WebRequest -Uri !repo!/.config/obs/global.ini -OutFile '%ProgramFiles%\obs-studio\config\obs-studio\global.ini'"
PowerShell -Command "Invoke-WebRequest -Uri !repo!/.config/obs/basic/profiles/ReplayBuffer/basic.ini -OutFile '%ProgramFiles%\obs-studio\config\obs-studio\basic\profiles\ReplayBuffer\basic.ini'"
PowerShell -Command "Invoke-WebRequest -Uri !repo!/.config/obs/basic/profiles/ReplayBuffer/recordEncoder.json -OutFile '%ProgramFiles%\obs-studio\config\obs-studio\basic\profiles\ReplayBuffer\recordEncoder.json'"
PowerShell -Command "Invoke-WebRequest -Uri !repo!/.config/obs/basic/scenes/ReplayBuffer.json -OutFile '%ProgramFiles%\obs-studio\config\obs-studio\basic\scenes\ReplayBuffer.json'"

:: Script OBS Replay Folder
PowerShell -Command "" ^
  "$archive = '%TEMP%\obs-replay-folders.zip';" ^
  "Invoke-WebRequest -Uri https://github.com/Mylloon/obs-replay-folders/releases/latest/download/obs-replay-folders.zip -OutFile $archive;" ^
  "Expand-Archive -Path $archive -DestinationPath '%ProgramFiles%\obs-studio\config' -Force;" ^
  "Remove-Item -Force $archive"

:: Script OBS Automatic ReplayBuffer Restart
@REM PowerShell -Command "" ^
@REM   "$archive = '%TEMP%\auto-obs-rb-restart.zip';" ^
@REM   "Invoke-WebRequest -Uri https://git.mylloon.fr/Anri/auto-obs-rb-restart/releases/download/latest/auto-obs-rb-restart.zip -OutFile $archive;" ^
@REM   "Expand-Archive -Path $archive -DestinationPath '%ProgramFiles%\obs-studio\config' -Force;" ^
@REM   "Remove-Item -Force $archive"

:: Setup the autostart of shadowplay
SCHTASKS /Create /RU "%USERNAME%" /SC ONLOGON /TN "OBS Shadowplay" ^
  /TR "'%LOCALAPPDATA%\start_obs_as_shadowplay.bat'" ^
  /F /RL HIGHEST

:: Allow system to go in sleep mode when OBS is running
POWERCFG /REQUESTSOVERRIDE PROCESS obs64.exe DISPLAY SYSTEM AWAYMODE

ECHO About OBS, you may need to adjust the screen ID by opening the OBS app
ECHO in the systray as it may be not the same as the one presaved.


:: Fix games shortcut
PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/apply_mihoyo_shortcut.bat -OutFile '%TEMP%\apply_mihoyo_shortcut.bat'"
CMD /c "%TEMP%\apply_mihoyo_shortcut.bat" yes


:: === FiveM ==
PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/installers/fivem.bat -OutFile '%TEMP%\fivem.bat'"
CMD /c "%TEMP%\fivem.bat" yes


:: === Bloxstrap ==
ECHO Roblox shortcut creation...
PowerShell -Command "" ^
  "$WshShell = New-Object -comObject WScript.Shell;" ^
  "$Shortcut = $WshShell.CreateShortcut('!startmenu!\Roblox.lnk');" ^
  "$Shortcut.TargetPath = '%LOCALAPPDATA%\Bloxstrap\Bloxstrap.exe';" ^
  "$Shortcut.Save()"


:: === Captvty ==
PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/installers/captvty.bat -OutFile '%TEMP%\captvty.bat'"
CMD /c "%TEMP%\captvty.bat" yes


:: === Dolphin ==
PowerShell -Command "Invoke-WebRequest -Uri !repo!/windows/installers/dolphin.bat -OutFile '%TEMP%\dolphin.bat'"
CMD /c "%TEMP%\dolphin.bat" yes


:: === Drivers notice ==
ECHO Installs drivers, for a list of drivers, use UserDiag https://userdiag.com/download
ECHO For GPU driver uninstallation, use DDU. For NVidia GPU driver isntallation, use NVC.


:: === End message ==
ECHO ""
ECHO About WSL, look at this: https://github.com/yuk7/ArchWSL/releases
ECHO ""
ECHO Take a look at your Windows settings, after you've done all the updates available on your PC.
ECHO Lastly, restart the computer.

PAUSE
EXIT /B
