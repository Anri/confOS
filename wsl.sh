#!/usr/bin/env bash

set -o errexit  # crash the script when a command crash
set -o pipefail # same as above for piped command
set -o nounset  # crash when a variable doesnt exist

# TRACE=1 for debug
if [[ "${TRACE-0}" == "1" ]]; then
  set -o xtrace
fi

cd "$(dirname "$0")" # change script directory

function main {
  # === Variables ==
  local repo
  repo=https://git.mylloon.fr/Anri/confOS/raw/branch/main


  # === Init pacman ==
  sudo pacman-key --init
  sudo pacman-key --populate
  sudo pacman -Sy --noconfirm archlinux-keyring


  # === Update Arch ==
  sudo pacman -Syyuu --noconfirm


  # === Install packages ==
  sudo pacman -S --noconfirm \
    git wget zip openssh bat base-devel make python-pygments micro git-lfs npm \
    sdl2_image nodejs fish autoconf valgrind automake python-virtualenv gdb \
    tk sdl2_mixer eza fzf glu man-pages-fr uwufetch mdcat python-pip opam \
    cpanminus unison timidity++ rebuild-detector difftastic sdl2_ttf rustup \
    libsamplerate rsync doxygen otf-fira-sans ttf-fira-code texlive-binextra \
    otf-fira-mono mingw-w64-gcc otf-font-awesome soundfont-fluid fluidsynth \
    pulseaudio-alsa texlive-latexextra texlive-langfrench texlive-luatex \
    noto-fonts jdk-openjdk texlive-fontsrecommended texlive-publishers sfml \
    texlive-fontsextra perl-yaml-tiny perl-file-homedir perl-file-which \
    man-pages bottom maven fakeroot texlive-mathscience texlive-plaingeneric \
    qpdf inkscape ffmpeg texlive-bibtexextra biber


  # === Remove SU password ==
  echo -e "\n# Disable password for specific user\n$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers > /dev/null


  # === Global OS configuration set-up ==
  curl ${repo}/meta.sh -s | bash -s -- ${repo}


  # === AUR packages ==
  paru -S --noconfirm \
    otf-font-awesome storrent-git


  # === Set-up systemd ==
  mkdir -p subsystemctl
  cd subsystemctl || exit
  curl -so PKGBUILD https://raw.githubusercontent.com/sorah/arch.sorah.jp/master/aur-sorah/PKGBUILDs/subsystemctl/PKGBUILD
  makepkg -si --noconfirm
  cd ..
  rm -rf subsystemctl ||:
  sudo subsystemctl start


  # === Locales ==
  sudo sed -i "s/#fr_FR.UTF-8 UTF-8/fr_FR.UTF8 UTF-8/g" /etc/locale.gen
  sudo locale-gen
  sudo subsystemctl stop; sudo subsystemctl start # fix weird bug
  subsystemctl exec sudo localectl set-locale fr_FR.UTF-8


  # === Shared folders ==
  sudo mkdir -p /mnt/y
  sudo mkdir -p /mnt/z


  # === Fish ==
  # Custom WSL configuration
  curl -s ${repo}/.config/fish/config_wsl.fish >> "$HOME"/.config/fish/config.fish
  curl -s ${repo}/.config/fish/conf.d/abbr_wsl.fish >> "$HOME"/.config/fish/conf.d/abbr.fish
  curl -s ${repo}/.config/fish/conf.d/alias_wsl.fish >> "$HOME"/.config/fish/conf.d/alias.fish
  # Specific WSL functions
  functions=("trash" "trash-empty")
  for function in "${functions[@]}"
  do
    wget -q "${repo}"/.config/fish/functions/"${function}".fish -O "$HOME"/.config/fish/functions/"${function}".fish
  done


  # === OCaml ==
  # Fix dlllwt_unix_stubs.so shared object file
  fish -c "set -Ux CAML_LD_LIBRARY_PATH $(opam var stublibs)"


  # === Valgrind ==
  fish -c "set -Ux DEBUGINFOD_URLS https://debuginfod.archlinux.org"


  # === Unison ==
  mkdir -p "$HOME"/.unison
  wget -q ${repo}/.unison/default.prf -O "$HOME"/.unison/default.prf


  # === Bash VSCode fix ==
  curl -s ${repo}/dotfiles/.bashrc >> "$HOME"/.bashrc


  # === Clear useless packages ==
  fish -c "pacman-clean"
  paru --noconfirm -Rsndd \
    subsystemctl fakeroot-tcp

  echo -e "\nInstallation terminée !\nIl faut redémarrer WSL (dans Powershell = wsl --shutdown)."
  echo -e "---\nPense bien à paramétrer ton terminal, exemple avec Windows Terminal :"
  echo    "- Ligne de commande = wsl.exe -d Arch fish"
  echo    "- Apparence :"
  echo    "   - Modifier la configuration avec https://git.mylloon.fr/Anri/confOS/wiki/windows-terminal"
  echo    "   - Type de police = Caskaydia Cove Nerd Font Complete Mono Windows Compatible"
  echo    "                      (à télécharger ici : https://www.nerdfonts.com/font-downloads)"
}

if grep "WSL2" /proc/version > /dev/null;
then
  main "$@"
else
  echo "Ce script ne fonctionne que sur ArchWSL." >&2
fi
